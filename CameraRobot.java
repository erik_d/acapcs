// uncomment Java package declaration when using the PDE


public class CameraRobot extends PCSObject {
	private String robotName;
	private double robotXPosition, robotYPosition, robotDirection;
	private long robotTimestamp;

	public CameraRobot() {
		robotName = "null";
		robotXPosition = -1;
		robotYPosition = -1;
		robotDirection = -1;
		this.robotTimestamp = -1;
	}
	
	public CameraRobot(String name, double xPos, double yPos, double phi, long timestamp) {
		robotName = name;
		robotXPosition = xPos;
		robotYPosition = yPos;
		robotDirection = phi;
		this.robotTimestamp = timestamp;
//		super.setTimerCount(count);
	}

	public void setName(String name) {
		robotName = name;
	}
	@Override
	public String getName() {
		return robotName;
	}
	@Override
	public void setXPosition(double xPos) {
		robotXPosition = xPos;
	}
	@Override
	public double getXPosition() {
		return robotXPosition;
	}
	@Override
	public void setYPosition(double yPos) {
		robotYPosition = yPos;
	}
	@Override
	public double getYPosition() {
		return robotYPosition;
	}

	public void setDirection(double phi) {
		robotDirection = phi;
	}
	@Override
	public double getDirection() {
		return robotDirection;
	}

	public void setTimestamp(long timestamp) {
		robotTimestamp = timestamp;
	}

	public double getTimestamp() {
		return robotTimestamp;
	}

	@Override
	public String toString() {
		return "||CameraRobot|| " + getName() + " / x-Pos: " + getXPosition() + " / y-Pos: " + getYPosition() + 
				" / phi: " + getDirection() + " / time:" + getTimestamp() + " / ";
	}
}