acaPCS - Process Control System for URGino robots

eclipse -> Processing PDE
! G4P library must be installed through the PDE
1. copy source files from "/src/" to "/" 
2. Create a new sketch in Processing, name it "acaPCS" and save it 
3. copy contens of "Main.java" to "acaPCS.pde"
3. uncomment class declaration and "(String[] args)" in "acaPCS.pde"
4. run

#acaPCS folder structure:

java:
data/ - shared with processing
libraries/
src/

eclipse:
.settings/

processing:
code/ - external libraries
data/ - shared with java

source files:
acaPCSpro.pde
CameraObject.java
CameraRobot.java
EncoderRobot.java
PCSController.java
PCSModel.java
PCSObject.java
PCSView.java
Tuple.java
