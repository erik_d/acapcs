public class EncoderRobot extends PCSObject {
	private double eRobotXPosition, eRobotYPosition, eRobotDirection, encoderPulsesLeft, encoderPulsesRight;
	private long timestampPose, timestampDrive, timestampObstacle;
	private double[] beamDistArray;

	public EncoderRobot(double xPos, double yPos, double phi, long timestampPose, double pulsesLeft, double pulsesRight, long timestampDrive, 
			double[] beamDistArray, long timestampObstacle) 
	{
		eRobotXPosition = xPos;
		eRobotYPosition = yPos;
		eRobotDirection = phi;
		this.timestampPose = timestampPose;
		encoderPulsesLeft = pulsesLeft;
		encoderPulsesRight = pulsesRight;
		this.timestampDrive = timestampDrive;
		this.beamDistArray = beamDistArray;
		this.timestampObstacle = timestampObstacle;
	}
	
	/**
	 * Standard Constructor for testing purposes
	 */
	public EncoderRobot(boolean dummy) {
		if(dummy) {
			eRobotXPosition = 0;
			eRobotYPosition = 0;
			eRobotDirection = 0;
			this.timestampPose = 42l;
			encoderPulsesLeft = 42d;
			encoderPulsesRight = 42d;
			this.timestampDrive = 42l;
			beamDistArray = new double[9];
			for(int i = 0; i < beamDistArray.length; i++) {
				beamDistArray[i] = 75d;
			}
			this.timestampObstacle = 42l;
		}
	}
	
	@Override
	public void setXPosition(double xPos) {
		eRobotXPosition = xPos;
	}
	@Override
	public double getXPosition() {
		return eRobotXPosition;
	}
	@Override
	public void setYPosition(double yPos) {
		eRobotYPosition = yPos;
	}
	@Override
	public double getYPosition() {
		return eRobotYPosition;
	}

	public void setDirection(double phi) {
		eRobotDirection = phi;
	}
	@Override
	public double getDirection() {
		return eRobotDirection;
	}
	
	public void setTimestampPose(long timestamp) {
		timestampPose = timestamp;
	}
	
	public double getTimestampPose() {
		 return timestampPose;
	}
	
	public void setPulsesLeft(double pLeft) {
		encoderPulsesLeft = pLeft;
	}

	public double getPulsesLeft() {
		return encoderPulsesLeft;
	}
	
	public void setPulsesRight(double pRight) {
		encoderPulsesRight = pRight;
	}

	public double getPulsesRight() {
		return encoderPulsesRight;
	}
	
	public void setTimestampDrive(long timestamp) {
		timestampDrive = timestamp;
	}
	
	public double getTimestampDrive() {
		 return timestampDrive;
	}
	
	public void setBeamDistArray(double[] beamdistArray) {
		beamDistArray = beamdistArray;
	}
	
	public double[] getBeamDistArray() {
		return beamDistArray;
	}

	// beamDistArray Format:
	// [0    , 1    , 2    , 3    , 4  , 5    , 6    , 7    , 8    ]
	// [lbd30, lbd60, lbd90, lbd45, cbd, rbd30, rbd60, rbd90, rbd45]
	public double getBeamDistLeft30() {
		return beamDistArray[0];
	}
	
	public double getBeamDistLeft60() {
		return beamDistArray[1];
	}
	
	public double getBeamDistLeft90() {
		return beamDistArray[2];
	}
	
	public double getBeamDistLeft45() {
		return beamDistArray[3];
	}
	
	public double getBeamDistCenter() {
		return beamDistArray[4];
	}
	
	public double getBeamDistRight30() {
		return beamDistArray[5];
	}
	
	public double getBeamDistRight60() {
		return beamDistArray[6];
	}
	
	public double getBeamDistRight90() {
		return beamDistArray[7];
	}
	
	public double getBeamDistRight45() {
		return beamDistArray[8];
	}
	
	public void setTimestampObstacle(long timestamp) {
		timestampObstacle = timestamp;
	}
	
	public double getTimestampObstacle() {
		 return timestampObstacle;
	}

	@Override
	public String toString() {
		return "||EncoderRobot|| x-Pos: " + getXPosition() + " / y-Pos: " + getYPosition() + " / phi: " + getDirection() + " / timePose: " + getTimestampPose() + 
				" | pulsesLeft: " + getPulsesLeft() + " / pulses Right: " + getPulsesRight() + " / timeDrive: " + getTimestampDrive() + " | beamLeft45°: " + 
				getBeamDistLeft45() + " / beamCenter: " + getBeamDistCenter() + " / beamRight45°: " + getBeamDistRight45() + "/ timeObstacle: " + 
				getTimestampObstacle() + " / ";
	}
}