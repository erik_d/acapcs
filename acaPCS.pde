import g4p_controls.GButton;
import g4p_controls.GDropList;
import g4p_controls.GEvent;
import g4p_controls.GImageButton;
import g4p_controls.GPanel;
import g4p_controls.GToggleControl;
import processing.core.PApplet;

/** 
 * Starting class, extends Processings main class PApplet. 
 * !!! Is only needed when using eclipse, uncomment class declaration when using the PDE. !!!
 */
//public class Main extends PApplet {
	
	private PCSModel pcsModel;
	private PCSView pcsView;
	private PCSController pcsController;
	
	/**
	 * Java main-method and starting point.
	 * Is only needed when using eclipse, uncomment when using the PDE.
	 * @author Erik Dobberkau
	 */
//	public static void main(String[] args) {
//		PApplet.main(Main.class.getName());
//	}
	
	/**
	 * Processings settings-method. Is called first and holds the size-method for setting the resolution.
	 */
	@Override
	public void settings() {
		size(1364, 742); // ("16:9") | (1366 - 2 Px, 768 - 26 Px on Mint) MINIMUM
//		size(1920, 1080) // (16:9) standard
//		fullScreen();
	}
	
	/**
	 * Processings setup-method. Is called once, after settings().
	 */
	@Override
	public void setup() {
		frameRate(30);
		smooth();
		pcsModel = new PCSModel();
		pcsView = new PCSView(this);
		pcsController = new PCSController(this, pcsModel, pcsView);
	}
	
	/**
	 * Processings draw-method. Is called x times per second. set in frameRate(x).
	 */
	@Override
	public void draw() {
		pcsController.updateView();
		pcsView.drawView();
	}
	
	/**
	 * G4P event handler method for buttons. Is passed to Controller.
	 * @param button The G4P element which fired the event.
	 * @param event The G4P event which was fired.
	 */
	public void handleButtonEvents(GButton button, GEvent event) {
		pcsController.handleMyButtonEvents(button, event);
	}
	
	/**
	 * G4P event handler method for checkboxes. Is passed to Controller.
	 * @param checkbox The G4P element which fired the event.
	 * @param event The G4P event which was fired.
	 */
	public void handleToggleControlEvents(GToggleControl checkbox, GEvent event) {
		pcsController.handleMyToggleControlEvents(checkbox, event);
	}
	
	/**
	 * G4P event handler method for droplists. Is passed to Controller.
	 * @param list The G4P element which fired the event.
	 * @param event The G4P event which was fired.
	 */
	public void handleDropListEvents(GDropList list, GEvent event) {
		pcsController.handlMyDropListEvents(list, event);
	}
	
	/**
	 * G4P event handler method for panels.
	 * @param panel The G4P element which fired the event.
	 * @param event The G4P event which was fired.
	 */
	public void handlePanelEvents(GPanel panel, GEvent event) { /* code */ }
	
	public void handleButtonEvents(GImageButton button, GEvent event) { /* code */ }

//}
