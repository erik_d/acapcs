// uncomment Java package declaration when using the PDE


public class Tuple<K, V> {
	public final K key;
	public final V value;

	public Tuple() {
		this.key = null;
		this.value = null;
	}
	
	public Tuple(K k, V v) {
		this.key = k;
		this.value = v;
	}
}
