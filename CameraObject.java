// uncomment Java package declaration when using the PDE


public class CameraObject extends PCSObject{
	private int objectColorcode, objectWidth, objectHeight;
	private double objectXPosition, objectYPosition;
	private long objectTimestamp;

	public CameraObject(int colorcode, double xPos, double yPos, int width, int height, long timestamp) {
		objectColorcode = colorcode;
		objectXPosition = xPos;
		objectYPosition = yPos;
		objectWidth = width;
		objectHeight = height;
		objectTimestamp = timestamp;
	}

	public void setColorCode(int colorcode) {
		objectColorcode = colorcode;
	}

	public int getColorCode() {
		return objectColorcode;
	}
	@Override
	public void setXPosition(double xPos) {
		objectXPosition = xPos;
	}
	@Override
	public double getXPosition() {
		return objectXPosition;
	}
	@Override
	public void setYPosition(double yPos) {
		objectYPosition = yPos;
	}
	@Override
	public double getYPosition() {
		return objectYPosition;
	}

	public void setWidth(int width) {
		objectWidth = width;
	}
	
	public int getWidth() {
		return objectWidth;
	}

	public void setHeight(int height) {
		objectHeight = height;
	}
	
	public int getHeight() {
		return objectHeight;
	}

	public void setTimeStamp(long timestamp) {
		objectTimestamp = timestamp;
	}

	public double getTimeStamp() {
		return objectTimestamp;
	}
	
	public String toString() {
		return "Object  || objectColorcode: " + getColorCode() + " | x-Pos: " + getXPosition() + " / y-Pos: " + getYPosition() + 
				" / width: " + getWidth() + " / height: " + getHeight() + " | Zeit:" + getTimeStamp() + " ||";
	}
}