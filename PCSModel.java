import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Map;

public class PCSModel {
	private Tuple<Long, Map<Integer, CameraRobot>> cameraRobotsTuple;
	private Tuple<Long, LinkedList<CameraObject>> cameraObjectStorageList;
	private Tuple<Long, EncoderRobot> encoderRobotTuple1, encoderRobotTuple2, encoderRobotTuple3;
	private LinkedList<Tuple<Long, PCSObject>> pcsObjectQueue;
	private boolean addCameraRobot1ToQueue, addCameraRobot2ToQueue, addCameraRobot3ToQueue, addEncoderRobot1ToQueue, addEncoderRobot2ToQueue, addEncoderRobot3ToQueue;
	public static boolean hasNewCameraRobots, hasNewCameraObjects, hasNewEncoderRobot1, hasNewEncoderRobot2, hasNewEncoderRobot3;
	
	public PCSModel() {
		addCameraRobot1ToQueue = addCameraRobot2ToQueue = addCameraRobot3ToQueue = addEncoderRobot1ToQueue = addEncoderRobot2ToQueue = addEncoderRobot3ToQueue = false;
		hasNewCameraRobots = hasNewCameraObjects = hasNewEncoderRobot1 = hasNewEncoderRobot2 = hasNewEncoderRobot3 = false;
		cameraRobotsTuple = new Tuple<Long, Map<Integer, CameraRobot>>();
		cameraObjectStorageList = new Tuple<Long, LinkedList<CameraObject>>();
		encoderRobotTuple1 = encoderRobotTuple2 = encoderRobotTuple3 = new Tuple<Long, EncoderRobot>();
		pcsObjectQueue = new LinkedList<Tuple<Long, PCSObject>>();
	}
	
	// -> map key 1-3: URGino 1-3 / value: CameraRobot# or null
	public synchronized Tuple<Long, Map<Integer, CameraRobot>> manageCameraRobots(Tuple<Long, Map<Integer, CameraRobot>> cRobotsTuple) {
		if(cRobotsTuple == null) { 	// get
			hasNewCameraRobots = false;
			return cameraRobotsTuple;
		} else {					// set
			hasNewCameraRobots = true;
			this.cameraRobotsTuple = cRobotsTuple;
			
			if(addCameraRobot1ToQueue) {
				Tuple<Long, PCSObject> cRobot1 = new Tuple<Long, PCSObject>(cRobotsTuple.key, cRobotsTuple.value.get(1));
				managePCSObjectRecording(cRobot1, "store");
			} else if(addCameraRobot2ToQueue) {
				Tuple<Long, PCSObject> cRobot2 = new Tuple<Long, PCSObject>(cRobotsTuple.key, cRobotsTuple.value.get(2));
				managePCSObjectRecording(cRobot2, "store");
			} else if(addCameraRobot3ToQueue) {
				Tuple<Long, PCSObject> cRobot3 = new Tuple<Long, PCSObject>(cRobotsTuple.key, cRobotsTuple.value.get(3));
				managePCSObjectRecording(cRobot3, "store");
			}
		}
		return null;
	}
	
	public synchronized Tuple<Long, EncoderRobot> manageEncoderRobot1(Tuple<Long, EncoderRobot> eRobotTuple) {
		if (eRobotTuple == null) { 	// get
			hasNewEncoderRobot1 = false;
			return encoderRobotTuple1;
		} else {				// set
			hasNewEncoderRobot1 = true;
			this.encoderRobotTuple1 = eRobotTuple;
			
			if(addEncoderRobot1ToQueue) {
				Tuple<Long, PCSObject> eRobot = new Tuple<Long, PCSObject>(eRobotTuple.key, eRobotTuple.value);
				managePCSObjectRecording(eRobot, "store");
			}
		}
		return null;
	}
	
	public synchronized Tuple<Long, EncoderRobot> manageEncoderRobot2(Tuple<Long, EncoderRobot> eRobotTuple) {
		if (eRobotTuple == null) { 	// get
			hasNewEncoderRobot2 = false;
			return encoderRobotTuple2;
		} else {				// set
			hasNewEncoderRobot2 = true;
			this.encoderRobotTuple2 = eRobotTuple;
			
			if(addEncoderRobot2ToQueue) {
				Tuple<Long, PCSObject> eRobot = new Tuple<Long, PCSObject>(eRobotTuple.key, eRobotTuple.value);
				managePCSObjectRecording(eRobot, "store");
			}
		}
		return null;
	}
	
	public synchronized Tuple<Long, EncoderRobot> manageEncoderRobot3(Tuple<Long, EncoderRobot> eRobotTuple) {
		if (eRobotTuple == null) { 	// get
			hasNewEncoderRobot3 = false;
			return encoderRobotTuple3;
		} else {				// set
			hasNewEncoderRobot3 = true;
			this.encoderRobotTuple3 = eRobotTuple;
			
			if(addEncoderRobot3ToQueue) {
				Tuple<Long, PCSObject> eRobot = new Tuple<Long, PCSObject>(eRobotTuple.key, eRobotTuple.value);
				managePCSObjectRecording(eRobot, "store");
			}
		}
		return null;
	}
	
	public synchronized Tuple<Long, LinkedList<CameraObject>> manageCameraObjects(Tuple<Long, LinkedList<CameraObject>> cObjectList) {
		if(cObjectList == null) { 	// get
			hasNewCameraObjects = false;
			return cameraObjectStorageList;
		} else { 					// set
			hasNewCameraObjects = true;
			this.cameraObjectStorageList = cObjectList;
		}
		return null;
	}
	
	private void writeRobotDataToFile() {
		String filename = "URGinoRecord.txt";
		BufferedWriter bufferedWriter = null;
		try {
			
			File file = new File(filename);
			if(!file.exists()) {
				file.createNewFile();
			}
			FileWriter fileWriter = new FileWriter(file);
			bufferedWriter = new BufferedWriter(fileWriter);
			for(Tuple<Long, PCSObject> robot : pcsObjectQueue) {
				try {
					if(robot.value != null) {
						bufferedWriter.write("update-count: " + robot.key + " " + robot.value.toString() + "\n");
					} else {
						bufferedWriter.write("update-count: " + robot.key + "\n");
					}
				} catch(IOException e) {
					System.err.println("Error writing File: " + filename + " (" + e.getMessage() + ")");
				}
			}
		} catch(IOException e) {
			System.err.println("Error writing File: " + filename + " (" + e.getMessage() + ")");
		} finally {
			try {
				if(bufferedWriter != null) {
					bufferedWriter.flush();
					bufferedWriter.close();
				}
			} catch(IOException e) {
				System.err.println("Error closing writer with file: " + filename + " (" + e.getMessage() + ")");
			}
		}
	}
	
	protected synchronized void managePCSObjectRecording(Tuple<Long, PCSObject> robot, String action) {
		switch (action) {
			case "store":
				pcsObjectQueue.add(robot);
				break;
			case "stop":
				addCameraRobot1ToQueue = addCameraRobot2ToQueue = addCameraRobot3ToQueue = addEncoderRobot1ToQueue = addEncoderRobot2ToQueue = addEncoderRobot3ToQueue = false;
				writeRobotDataToFile();
				pcsObjectQueue.clear();
				break;
			case "startc1":
				addCameraRobot1ToQueue = true;
				break;
			case "startc2":
				addCameraRobot2ToQueue = true;
				break;
			case "startc3":
				addCameraRobot3ToQueue = true;
				break;
			case "starte1":
				addEncoderRobot1ToQueue = true;
				break;
			case "starte2":
				addEncoderRobot2ToQueue = true;
				break;
			case "starte3":
				addEncoderRobot3ToQueue = true;
				break;
		}
	}
}