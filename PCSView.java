import java.awt.Font;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.LinkedList;
import java.util.Map;

import g4p_controls.FontManager;
import g4p_controls.GAlign;
import g4p_controls.GButton;
import g4p_controls.GCheckbox;
import g4p_controls.GDropList;
import g4p_controls.GImageButton;
import g4p_controls.GLabel;
import g4p_controls.GPanel;
import g4p_controls.GSketchPad;
import g4p_controls.GTextIconBase;
import processing.core.PApplet;
import processing.core.PGraphics;
import processing.core.PImage;
import processing.core.PShape;

/**
 * The View holds, builds and draws all GUI-elements. It gets updated by the Controller. It stores data necessary to draw and holds the View-logic.
 * @author Erik Dobberkau
 */
public class PCSView {
	
	private PApplet parent;
	private PGraphics pcsPGraphic, controlPGraphic;
	private GSketchPad pcsGSketchPad, controlGSketchPad;
	private GPanel controlContainerGPanel;
	// 
	private PImage pcsBackgroundPImage;
	private PShape svgBlueBird, svgGreenFalcon, svgRedDragon, svgDefault, svgEncoderRobot1, svgEncoderRobot2, svgEncoderRobot3, svgInactiveOverlay;
	private GLabel column1Heading, column2Heading, column3Heading, bot1NameGLabel, bot1CameraXPosGLabel, bot1EncoderXPosGLabel, bot1CameraYPosGLabel, 
		bot1EncoderYPosGLabel, bot1CameraDirectionGLabel, bot1EncoderDirectionGLabel, bot1EncoderDistanceGLabel, bot1EncoderCBeamGLabel, 
		bot1EncoderLBeam45GLabel, bot1EncoderRBeam45GLabel, bot1ShowOptionsGLabel, bot2NameGLabel, bot2CameraXPosGLabel, bot2EncoderXPosGLabel, bot2CameraYPosGLabel, 
		bot2EncoderYPosGLabel, bot2CameraDirectionGLabel, bot2EncoderDirectionGLabel, bot2EncoderDistanceGLabel, bot2EncoderCBeamGLabel, 
		bot2EncoderLBeam45GLabel, bot2EncoderRBeam45GLabel, bot2ShowOptionsGLabel, bot3NameGLabel, bot3CameraXPosGLabel, bot3EncoderXPosGLabel, bot3CameraYPosGLabel, 
		bot3EncoderYPosGLabel, bot3CameraDirectionGLabel, bot3EncoderDirectionGLabel, bot3EncoderDistanceGLabel, bot3EncoderCBeamGLabel, 
		bot3EncoderLBeam45GLabel, bot3EncoderRBeam45GLabel, bot3ShowOptionsGLabel, object1GLabel, object2GLabel, object3GLabel, object4GLabel, object1ColorGLabel, 
		object2ColorGLabel, object3ColorGLabel, object4ColorGLabel, object1XPosGLabel, object2XPosGLabel, object3XPosGLabel, object4XPosGLabel, 
		object1YPosGLabel, object2YPosGLabel, object3YPosGLabel, object4YPosGLabel, recordCameraHeadingGLabel, recordEncoderHeadingGLabel, updateHeadingGLabel;
	private GImageButton bot1IconGImageButton, bot2IconGImageButton, bot3IconGImageButton;
	private Font controlsFont;
	private NumberFormat robotXYFormat, robotPhiFormat, robotdistanceFormat, objectXYFormat;
	private String[] monoSpaced = {"Monospaced"};
	private float mazeWidthCm, mazeHeightCm, mazeWidthHalfCm, mazeHeightHalfCm, robotDiameterCm, robotDiameterPx, objectDiameterCm, objectDiameterPx, xPosLaserOrigin,
				  laserEndDotDiameter;
	private int pcsGSketchPadWidth, pcsGSketchPadHeight, pcsGSketchPadWidthHalf, pcsGSketchPadHeightHalf, controlGSketchPadHeight, controlGSketchPadWidth, 
				pcsGsketchPadXPosition, pcsGsketchPadYPosition, controlGPanelXPosition, controlGPanelYPosition, viewOuterBorder, controlSpacing, controlSpacingHalf, 
				controlColumn1Width, controlColumn2Width, controlColumn3Width, controlColumnStandardHeight, controlColumnHeadingHeight, column1BotXPos, column2BotXPos, 
				column3BotXPos, controlYPos, controlTextWidth, controlBotRectYPos, columnObjectWidth, column1ObjectXPos, column2ObjectXPos, column3ObjectXPos, 
				column4ObjectXPos, column5ObjectXPos, column1OptionsXPos, column2OptionsXPos, column3OptionsXPos, columnOptionsWidth, columnOptionsWidthEff, backGroundColor;
	private final String ROBOT_RED_DRAGON = "Red Dragon";
	private final String ROBOT_BLUE_BIRD = "Blue Bird";
	private final String ROBOT_GREEN_FALCON = "Green Falcon";
	public final String URGINO_1 = "URGino 1";
	public final String URGINO_2 = "URGino 2";
	public final String URGINO_3 = "URGino 3";
	public final String NO_SELECTION = "Select Source";
	public final int CENTER_MODE, RED, ORANGE, YELLOW, GREEN, CYAN, BLUE, GREY, COLOR_SCHEME;
	protected GImageButton recordGImageButton, recordIconGImageButtonRed, recordIconGImageButtonGreen, acaBotLogoGImageButton;
	protected GButton recordGButton;
	protected GCheckbox showBot1CameraGCheckbox, showBot1EncoderGCheckbox, showBot2CameraGCheckbox, showBot2EncoderGCheckbox, showBot3CameraGCheckbox, showBot3EncoderGCheckbox, 
						showObjectsGCheckbox, updateCameraDataCheckbox, updateBot1EncoderDataCheckbox, updateBot2EncoderDataCheckbox, updateBot3EncoderDataCheckbox, 
						drawEncoderUpFrontCheckbox, showBot1LaserGCheckbox, showBot2LaserGCheckbox, showBot3LaserGCheckbox;
	protected GDropList recordCameraGDropList, recordEncoderGDropList;
	
	private int maxStoredRobots, maxStoredObjects;
	public boolean drawEncoderUpFront, showERobot1Laser, showERobot2Laser, showERobot3Laser, filterMedian;
	private LinkedList<Tuple<Long, Map<Integer, CameraRobot>>> cRobotsList;
	private LinkedList<Tuple<Long, EncoderRobot>> eRobotList1, eRobotList2, eRobotList3;
	private LinkedList<Tuple<Long, LinkedList<CameraObject>>> cObjectsList;
	/**
	 * Constructor.
	 * @param parent The instance of the Processing main class PApplet which was passed to the View.
	 */
	PCSView(PApplet parent) {
		this.parent = parent;
		CENTER_MODE = 3;
		laserEndDotDiameter = 2f;
		RED = parent.color(255, 0, 0);
		ORANGE = parent.color(255, 165, 0);
		YELLOW = parent.color(255, 255, 0);
		GREEN = parent.color(0, 255, 0);
		CYAN = parent.color(0, 255, 255);
		BLUE = parent.color(0, 0, 255);
		GREY = parent.color(80);
 
		// Storage lists to store the last received updates (View-Model).
		cRobotsList = new LinkedList<Tuple<Long, Map<Integer, CameraRobot>>>();
		eRobotList1 = new LinkedList<Tuple<Long, EncoderRobot>>();
		eRobotList2 = new LinkedList<Tuple<Long, EncoderRobot>>();
		eRobotList3 = new LinkedList<Tuple<Long, EncoderRobot>>();
		cObjectsList = new LinkedList<Tuple<Long, LinkedList<CameraObject>>>();
		drawEncoderUpFront = true;
		showERobot1Laser = showERobot2Laser = showERobot3Laser = false;
		//%%%%% G4P blue color scheme = 6 %%%%%//
		COLOR_SCHEME = 6;
		//%%%%% defines how many subsequent update frames are stored in the View-Model %%%%%//
		maxStoredRobots = 10;
		maxStoredObjects = 1;
		
		buildView();
	}
	
	/**
	 * Creates all ui-elements.
	 */
	private void buildView() {
		//%%%%% sets the maze background image and color %%%%%//
		pcsBackgroundPImage = parent.loadImage("maze.jpg"); // Images: "maze.jpg", "appartment.jpg"
		backGroundColor = parent.color(160);
		
		robotXYFormat = new DecimalFormat(" #0.0;-#0.0");
		robotPhiFormat = new DecimalFormat(" #0.0;- #0.0");
		robotdistanceFormat = new DecimalFormat(" ##0.0");
		objectXYFormat = new DecimalFormat(" #0;-#0");
		
		mazeWidthCm = 152.0f;	// width of the maze including the border in cm
		mazeHeightCm = 152.0f;	// height of the maze including the border in cm
		mazeWidthHalfCm = mazeWidthCm / 2;
		mazeHeightHalfCm = mazeHeightCm / 2;
		robotDiameterCm = 14.0f;
		objectDiameterCm = 8.5f;
		viewOuterBorder = (int)Math.round(parent.height / 35.0);
		
		buildPCSSketchPad();
		buildControlPanel();
	}

	/**
	 * Updates the View-Model with new Camera Robots.
	 * @param cRobotsTuple Holds the updateCount and a map of the actual CameraRobots.
	 */
	public synchronized void updateCameraRobotList(Tuple<Long, Map<Integer, CameraRobot>> cRobotsTuple) {
		if(cRobotsList.size() >= maxStoredRobots) {
			cRobotsList.removeFirst();
		}
		cRobotsList.addLast(cRobotsTuple);
	}

	/**
	 * Updates the View-Model with new EncoderRobots 1.
	 * @param eRobotTuple Holds the updateCount and a map of the actual EncoderRobots.
	 */
	public synchronized void updateEncoderRobotList1(Tuple<Long, EncoderRobot> eRobotTuple) {
		if(eRobotList1.size() >= maxStoredRobots) {
			eRobotList1.removeFirst();
		}
		eRobotList1.addLast(eRobotTuple);
	}

	/**
	 * Updates the View-Model with new EncoderRobots 2.
	 * @param eRobotTuple Holds the updateCount and a map of the actual EncoderRobots.
	 */
	public synchronized void updateEncoderRobotList2(Tuple<Long, EncoderRobot> eRobotTuple) {
		if(eRobotList2.size() >= maxStoredRobots) {
			eRobotList2.removeFirst();
		}
		eRobotList2.addLast(eRobotTuple);
	}

	/**
	 * Updates the View-Model with new EncoderRobots 3.
	 * @param eRobotTuple Holds the updateCount and a map of the actual EncoderRobots.
	 */
	public synchronized void updateEncoderRobotList3(Tuple<Long, EncoderRobot> eRobotTuple) {
		if(eRobotList3.size() >= maxStoredRobots) {
			eRobotList3.removeFirst();
		}
		eRobotList3.addLast(eRobotTuple);
	}
	/**
	 * Updates the View-Model with new CameraObejcts.
	 * @param cObjectsTuple Holds the updateCount and a list of the actual CameraRobots.
	 */
	public synchronized void updateCameraObjectList(Tuple<Long, LinkedList<CameraObject>> cObjectsTuple) {
		if(cObjectsList.size() >= maxStoredObjects) {
			cObjectsList.removeFirst();
		}
		cObjectsList.addLast(cObjectsTuple);
	}
	
	/**
	 *  Processings draw-method. Is called from Processing Main-class
	 */
	public void drawView() {
		clearPCSGraphic();
		if(drawEncoderUpFront) {
			if(updateCameraDataCheckbox.isSelected()) {
				drawCameraRobots();
			}
			if(updateBot1EncoderDataCheckbox.isSelected()) {
				drawEncoderRobot(1);
			}
			if(updateBot2EncoderDataCheckbox.isSelected()) {
				drawEncoderRobot(2);
			}
			if(updateBot3EncoderDataCheckbox.isSelected()) {
				drawEncoderRobot(3);
			}
		} else {
			if(updateBot1EncoderDataCheckbox.isSelected()) {
				drawEncoderRobot(1);
			}
			if(updateBot2EncoderDataCheckbox.isSelected()) {
				drawEncoderRobot(2);
			}
			if(updateBot3EncoderDataCheckbox.isSelected()) {
				drawEncoderRobot(3);
			}
			if(updateCameraDataCheckbox.isSelected()) {
				drawCameraRobots();
			}
		}
		if(updateCameraDataCheckbox.isSelected()) {
			drawCameraObjects();
		}
	}
	
	/**
	 * Draws an empty pcs. Is called at the beginning of Processings draw-method.
	 */
	private void clearPCSGraphic() {
		parent.background(backGroundColor);
		pcsPGraphic.beginDraw();
			pcsPGraphic.image(pcsBackgroundPImage, 0 , 0, pcsGSketchPadWidth, pcsGSketchPadHeight);
		pcsPGraphic.endDraw();
	}

	/**
	 * Draws all CameraRobots that are present in the View-Model storageList.
	 */
	private synchronized void drawCameraRobots() {
		// draw URGino 1: Red Dragon
		if(!cRobotsList.isEmpty()) {
			if(filterMedian) {
				// ToDo: filter method call -> for each
			} else if(cRobotsList.getLast().value.containsKey(1)) {
				CameraRobot cRobot1 = cRobotsList.getLast().value.get(1);
				if(showBot1CameraGCheckbox.isSelected()) drawPCSRobot(cRobot1, svgRedDragon);
				setCameraRobot1ControlTextColor(-1);
				updateCameraRobot1ControlText(cRobot1);
			} else if(showBot1CameraGCheckbox.isSelected()) {
				boolean notFound = false;
				for(int i = cRobotsList.size() - 2; i >= 0; i--) {
					if(cRobotsList.get(i).value.containsKey(1)) {
						drawPCSRobot(cRobotsList.get(i).value.get(1), svgRedDragon);
						drawPCSRobot(cRobotsList.get(i).value.get(1), svgInactiveOverlay);
						notFound = false;
						break;
					} else {
						notFound = true;
					}
				}
				if(notFound) {
					setCameraRobot1ControlTextColor(GREY);
				}
			}
			// draw URGino 2: Blue Bird
			if(cRobotsList.getLast().value.containsKey(2)) {
				CameraRobot cRobot2 = cRobotsList.getLast().value.get(2);
				if(showBot2CameraGCheckbox.isSelected()) drawPCSRobot(cRobot2, svgBlueBird);
				setCameraRobot2ControlTextColor(-1);
				updateCameraRobot2ControlText(cRobot2);
			} else if(showBot2CameraGCheckbox.isSelected()) {
				boolean notFound = false;
				for(int i = cRobotsList.size() - 2; i >= 0; i--) {
					if(cRobotsList.get(i).value.containsKey(2)) {
						drawPCSRobot(cRobotsList.get(i).value.get(2), svgBlueBird);
						drawPCSRobot(cRobotsList.get(i).value.get(2), svgInactiveOverlay);
						notFound = false;
						break;
					} else {
						notFound = true;
					}
				}
				if(notFound) {
					setCameraRobot2ControlTextColor(GREY);
				}
			}
			// draw URGino 3: Green Falcon
			if(cRobotsList.getLast().value.containsKey(3)) {
				CameraRobot cRobot3 = cRobotsList.getLast().value.get(3);
				if(showBot3CameraGCheckbox.isSelected()) drawPCSRobot(cRobot3, svgGreenFalcon);
				setCameraRobot3ControlTextColor(-1);
				updateCameraRobot3ControlText(cRobot3);
			} else if(showBot3CameraGCheckbox.isSelected()) {
				boolean notFound = false;
				for(int i = cRobotsList.size() - 2; i >= 0; i--) {
					if(cRobotsList.get(i).value.containsKey(3)) {
						drawPCSRobot(cRobotsList.get(i).value.get(3), svgGreenFalcon);
						drawPCSRobot(cRobotsList.get(i).value.get(3), svgInactiveOverlay);
						notFound = false;
						break;
					} else {
						notFound = true;
					}
				}
				if(notFound) {
					setCameraRobot3ControlTextColor(GREY);
				}
			}
			// draw default URGino
			if(cRobotsList.getLast().value.containsKey(4)) {
				CameraRobot cRobot4 = cRobotsList.getLast().value.get(4);
				drawPCSRobot(cRobot4, svgDefault);
			} else {
				for(int i = cRobotsList.size() - 2; i >= 0; i--) {
					if(cRobotsList.get(i).value.containsKey(4)) {
						drawPCSRobot(cRobotsList.get(i).value.get(4), svgDefault);
						break;
					}
				}
			}
		}
	}

	/**
	 * Draws a specified EncoderRobot that is present in the View-Model storageList.
	 * @param id An int from 1-3 refering to URGino 1-3.
	 */
	private synchronized void drawEncoderRobot(int id) {
		switch (id) {
			case 1:
				if(!eRobotList1.isEmpty()) {
					EncoderRobot eRobot1 = eRobotList1.getLast().value;
					if(updateBot1EncoderDataCheckbox.isSelected() && eRobot1 != null) {
						if(showBot1EncoderGCheckbox.isSelected()) {
							drawPCSRobot(eRobot1, svgEncoderRobot1);
							if(showERobot1Laser) drawLaserScan(eRobot1);
						}
						setEncoderRobot1ControlTextColor(-1);
						updateEncoderRobot1ControlText(eRobot1);
					} else if(updateBot1EncoderDataCheckbox.isSelected()) {
						boolean notFound = false;
						for(int i = eRobotList1.size() - 2; i >= 0; i--) {
							if(eRobotList1.get(i).value != null) {
								if(showBot1EncoderGCheckbox.isSelected()) {
									drawPCSRobot(eRobotList1.get(i).value, svgEncoderRobot1);
									if(showERobot1Laser) drawLaserScan(eRobotList1.get(i).value);
									drawPCSRobot(eRobotList1.get(i).value, svgInactiveOverlay);
								}
								notFound = false;
								break;
							} else {
								notFound = true;
							}
						}
						if(notFound) {
							setEncoderRobot1ControlTextColor(GREY);
						}
					}
				}
				break;
			case 2:
				if(!eRobotList2.isEmpty()) {
					EncoderRobot eRobot2 = eRobotList2.getLast().value;
					if(updateBot2EncoderDataCheckbox.isSelected() && eRobot2 != null) {
						if(showBot2EncoderGCheckbox.isSelected()) {
							drawPCSRobot(eRobot2, svgEncoderRobot2);
							if(showERobot2Laser) drawLaserScan(eRobot2);
						}
						setEncoderRobot2ControlTextColor(-1);
						updateEncoderRobot2ControlText(eRobot2);
					} else if(updateBot2EncoderDataCheckbox.isSelected()) {
						boolean notFound = false;
						for(int i = eRobotList2.size() - 2; i >= 0; i--) {
							if(eRobotList2.get(i).value != null) {
								if(showBot2EncoderGCheckbox.isSelected()) {
									drawPCSRobot(eRobotList2.get(i).value, svgEncoderRobot2);
									if(showERobot2Laser) drawLaserScan(eRobotList2.get(i).value);
									drawPCSRobot(eRobotList2.get(i).value, svgInactiveOverlay);
								}
								notFound = false;
								break;
							} else {
								notFound = true;
							}
						}
						if(notFound) {
							setEncoderRobot2ControlTextColor(GREY);
						}
					}
				}
				break;
			case 3:
				if(!eRobotList3.isEmpty()) {
					EncoderRobot eRobot3 = eRobotList3.getLast().value;
					if(updateBot3EncoderDataCheckbox.isSelected() && eRobot3 != null) {
						if(showBot3EncoderGCheckbox.isSelected()) {
							drawPCSRobot(eRobot3, svgEncoderRobot3);
							if(showERobot3Laser) drawLaserScan(eRobot3);
						}
						setEncoderRobot3ControlTextColor(-1);
						updateEncoderRobot3ControlText(eRobot3);
					} else if(updateBot3EncoderDataCheckbox.isSelected()) {
						boolean notFound = false;
						for(int i = eRobotList3.size() - 2; i >= 0; i--) {
							if(eRobotList3.get(i).value != null) {
								if(showBot3EncoderGCheckbox.isSelected()) {
									drawPCSRobot(eRobotList3.get(i).value, svgEncoderRobot3);
									if(showERobot3Laser) drawLaserScan(eRobotList3.get(i).value);
									drawPCSRobot(eRobotList3.get(i).value, svgInactiveOverlay);
								}
								notFound = false;
								break;
							} else {
								notFound = true;
							}
						}
						if(notFound) {
							setEncoderRobot3ControlTextColor(GREY);
						}
					}
				}
				break;
		}
	}
	
	/**
	 * Draws laser scanner lines around the passed EncoderRobot object.
	 * @param eRobot The EncoderRobot Object which holds the pose and distances
	 */
	private void drawLaserScan(EncoderRobot eRobot) {
		pcsPGraphic.beginDraw();			
		pcsPGraphic.pushMatrix();
			pcsPGraphic.strokeWeight(1);
			pcsPGraphic.stroke(RED);
			pcsPGraphic.fill(RED);
			pcsPGraphic.ellipseMode(CENTER_MODE);
			pcsPGraphic.translate(pcsGSketchPadWidthHalf, pcsGSketchPadHeightHalf);
			pcsPGraphic.translate(convertXPositionToSketch(eRobot.getXPosition()), convertYPositionToSketch(eRobot.getYPosition()));
			pcsPGraphic.rotate(PApplet.radians(-(float)eRobot.getDirection()));
			xPosLaserOrigin = robotDiameterPx / 4;
			// draw center laser
			pcsPGraphic.line(xPosLaserOrigin, 0f, xPosLaserOrigin + convertXPositionToSketch(eRobot.getBeamDistCenter()), 0);
			pcsPGraphic.ellipse(xPosLaserOrigin + convertXPositionToSketch(eRobot.getBeamDistCenter()), 0, laserEndDotDiameter, laserEndDotDiameter);
			//draw left 30° laser
			pcsPGraphic.line(xPosLaserOrigin, 0f, xPosLaserOrigin + convertXPositionToSketch(Math.cos(Math.toRadians(30)) * eRobot.getBeamDistLeft30()), convertYPositionToSketch(Math.sin(Math.toRadians(30)) * eRobot.getBeamDistLeft30()));
			pcsPGraphic.ellipse(xPosLaserOrigin + convertXPositionToSketch(Math.cos(Math.toRadians(30)) * eRobot.getBeamDistLeft30()), convertYPositionToSketch(Math.sin(Math.toRadians(30)) * eRobot.getBeamDistLeft30()), laserEndDotDiameter, laserEndDotDiameter);
			// draw left 45° laser
			pcsPGraphic.line(xPosLaserOrigin, 0f, xPosLaserOrigin + convertXPositionToSketch(Math.cos(Math.toRadians(45)) * eRobot.getBeamDistLeft45()), convertYPositionToSketch(Math.sin(Math.toRadians(45)) * eRobot.getBeamDistLeft45()));
			pcsPGraphic.ellipse(xPosLaserOrigin + convertXPositionToSketch(Math.cos(Math.toRadians(45)) * eRobot.getBeamDistLeft45()), convertYPositionToSketch(Math.sin(Math.toRadians(45)) * eRobot.getBeamDistLeft45()), laserEndDotDiameter, laserEndDotDiameter);
			// draw left 60° laser
			pcsPGraphic.line(xPosLaserOrigin, 0f, xPosLaserOrigin + convertXPositionToSketch(Math.cos(Math.toRadians(60)) * eRobot.getBeamDistLeft60()), convertYPositionToSketch(Math.sin(Math.toRadians(60)) * eRobot.getBeamDistLeft60()));
			pcsPGraphic.ellipse(xPosLaserOrigin + convertXPositionToSketch(Math.cos(Math.toRadians(60)) * eRobot.getBeamDistLeft60()), convertYPositionToSketch(Math.sin(Math.toRadians(60)) * eRobot.getBeamDistLeft60()), laserEndDotDiameter, laserEndDotDiameter);
			//draw left 90° laser
			pcsPGraphic.line(xPosLaserOrigin, 0f, xPosLaserOrigin, -convertXPositionToSketch(eRobot.getBeamDistLeft90()));
			pcsPGraphic.ellipse(xPosLaserOrigin, -convertXPositionToSketch(eRobot.getBeamDistLeft90()), laserEndDotDiameter, laserEndDotDiameter);
			//draw right 30° laser
			pcsPGraphic.line(xPosLaserOrigin, 0f, xPosLaserOrigin + convertXPositionToSketch(Math.cos(Math.toRadians(30)) * eRobot.getBeamDistRight30()), -convertYPositionToSketch(Math.sin(Math.toRadians(30)) * eRobot.getBeamDistRight30()));
			pcsPGraphic.ellipse(xPosLaserOrigin + convertXPositionToSketch(Math.cos(Math.toRadians(30)) * eRobot.getBeamDistRight30()), -convertYPositionToSketch(Math.sin(Math.toRadians(30)) * eRobot.getBeamDistRight30()), laserEndDotDiameter, laserEndDotDiameter);
			// draw right 45° laser
			pcsPGraphic.line(xPosLaserOrigin, 0f, xPosLaserOrigin + convertXPositionToSketch(Math.cos(Math.toRadians(45)) * eRobot.getBeamDistRight45()), -convertYPositionToSketch(Math.sin(Math.toRadians(45)) * eRobot.getBeamDistRight45()));
			pcsPGraphic.ellipse(xPosLaserOrigin + convertXPositionToSketch(Math.cos(Math.toRadians(45)) * eRobot.getBeamDistRight45()), -convertYPositionToSketch(Math.sin(Math.toRadians(45)) * eRobot.getBeamDistRight45()), laserEndDotDiameter, laserEndDotDiameter);
			//draw right 60° laser
			pcsPGraphic.line(xPosLaserOrigin, 0f, xPosLaserOrigin + convertXPositionToSketch(Math.cos(Math.toRadians(60)) * eRobot.getBeamDistRight60()), -convertYPositionToSketch(Math.sin(Math.toRadians(60)) * eRobot.getBeamDistRight60()));
			pcsPGraphic.ellipse(xPosLaserOrigin + convertXPositionToSketch(Math.cos(Math.toRadians(60)) * eRobot.getBeamDistRight60()), -convertYPositionToSketch(Math.sin(Math.toRadians(60)) * eRobot.getBeamDistRight60()), laserEndDotDiameter, laserEndDotDiameter);
			// draw right 90° laser
			pcsPGraphic.line(xPosLaserOrigin, 0f, xPosLaserOrigin, convertXPositionToSketch(eRobot.getBeamDistRight90()));
			pcsPGraphic.ellipse(xPosLaserOrigin, convertXPositionToSketch(eRobot.getBeamDistRight90()), laserEndDotDiameter, laserEndDotDiameter);
		pcsPGraphic.popMatrix();
	pcsPGraphic.endDraw();
	}
	
	/**
	 * Draws the CameraObjects that are present in the View-Model
	 */
	private synchronized void drawCameraObjects() {
		if(updateCameraDataCheckbox.isSelected() && !cObjectsList.isEmpty()) {
			LinkedList<Tuple<Integer, CameraObject>> latestObjects = getLastCameraObjects();
			boolean notFound1, notFound3, notFound4, notFound6;
			notFound1 = notFound3 = notFound4 = notFound6 = true;
			int colorCode = 0;
			
			for(Tuple<Integer, CameraObject> object : latestObjects) {
				colorCode = object.value.getColorCode();
				switch (colorCode) {
				case 1:
					if(object.key == 0) {
						if(showObjectsGCheckbox.isSelected()) drawPCSObject(object.value, RED);
						setCameraObject1ControlTextColor(-1);
						updateCameraObject1ControlText(object.value, "Red");
					} else if(showObjectsGCheckbox.isSelected()) {
							drawPCSObject(object.value, RED);
							drawPCSObjectOverlay(object.value, svgInactiveOverlay);
					}
					notFound1 = false;
					break;
				case 2:
					if(showObjectsGCheckbox.isSelected()) drawPCSObject(object.value, ORANGE);
					break;
				case 3:
					if(object.key == 0) {
						if(showObjectsGCheckbox.isSelected()) drawPCSObject(object.value, YELLOW);
						setCameraObject2ControlTextColor(-1);
						updateCameraObject2ControlText(object.value, "Yellow");
					} else if(showObjectsGCheckbox.isSelected()) {
						drawPCSObject(object.value, YELLOW);
						drawPCSObjectOverlay(object.value, svgInactiveOverlay);
					}
					notFound3 = false;
					break;
				case 4:
					if(object.key == 0) {
						if(showObjectsGCheckbox.isSelected()) drawPCSObject(object.value, GREEN);
						setCameraObject3ControlTextColor(-1);
						updateCameraObject3ControlText(object.value, "Green");
					} else if(showObjectsGCheckbox.isSelected()) {
						drawPCSObject(object.value, GREEN);
						drawPCSObjectOverlay(object.value, svgInactiveOverlay);
					}
					notFound4 = false;
					break;
				case 5:
					if(showObjectsGCheckbox.isSelected()) drawPCSObject(object.value, CYAN);
					break;
				case 6:
					if(object.key == 0) {
						if(showObjectsGCheckbox.isSelected()) drawPCSObject(object.value, BLUE);
						setCameraObject4ControlTextColor(-1);
						updateCameraObject4ControlText(object.value, "Blue");
					} else if(showObjectsGCheckbox.isSelected()) {
						drawPCSObject(object.value, BLUE);
						drawPCSObjectOverlay(object.value, svgInactiveOverlay);
					}
					notFound6 = false;
					break;
				}
			}
			if(notFound1) setCameraObject1ControlTextColor(GREY);
			if(notFound3) setCameraObject2ControlTextColor(GREY);
			if(notFound4) setCameraObject3ControlTextColor(GREY);
			if(notFound6) setCameraObject4ControlTextColor(GREY);
		}
	}
	
	/**
	 * Searches the View-Model for the last active CameraObjects and their skipped update counts.
	 * @return LinkedList of Tuples consisting of update-skip and CameraObject.
	 */
	private synchronized LinkedList<Tuple<Integer, CameraObject>> getLastCameraObjects() {
		LinkedList<Tuple<Integer, CameraObject>> lastObjects = new LinkedList<Tuple<Integer, CameraObject>>();
		int colorCode = 0;
		int skip = -1;
		long latestCount = 0;
		latestCount = cObjectsList.getLast().key;
		boolean notFound1, notFound3, notFound4, notFound6;
		notFound1 = notFound3 = notFound4 = notFound6 = true;
		
		for(int i = cObjectsList.size() - 1; i >= 0; i--) {
			for(CameraObject obj : cObjectsList.get(i).value) {
				colorCode = obj.getColorCode();
				switch (colorCode) {
				case 1: // red
					if(i + 1 == maxStoredObjects) { // latest update
						skip = 0;
						lastObjects.add(new Tuple<Integer, CameraObject>(skip, obj));
						notFound1 = false;
					} else if(notFound1) { // search the storage-list for older occurrences
						skip = (int)(latestCount - cObjectsList.get(i).key);
						lastObjects.add(new Tuple<Integer, CameraObject>(skip, obj));
						notFound1 = false;
					}
					break;
				case 2: // orange
					if(i + 1 == maxStoredObjects) { // latest update
						skip = 0;
						lastObjects.add(new Tuple<Integer, CameraObject>(skip, obj));
					}
					break;
				case 3: // yellow
					if(i + 1 == maxStoredObjects) { // latest update
						skip = 0;
						lastObjects.add(new Tuple<Integer, CameraObject>(skip, obj));
						notFound3 = false;
					} else if(notFound3) { // search the storage-list for older occurrences
						skip = (int)(latestCount - cObjectsList.get(i).key);
						lastObjects.add(new Tuple<Integer, CameraObject>(skip, obj));
						notFound3 = false;
					}
					break;
				case 4: // green
					if(i + 1 == maxStoredObjects) { // latest update
						skip = 0;
						lastObjects.add(new Tuple<Integer, CameraObject>(skip, obj));
						notFound4 = false;
					} else if(notFound4) { // search the storage-list for older occurrences
						skip = (int)(latestCount - cObjectsList.get(i).key);
						lastObjects.add(new Tuple<Integer, CameraObject>(skip, obj));
						notFound4 = false;
					}
					break;
				case 5: // cyan
					if(i + 1 == maxStoredObjects) { // latest update
						skip = 0;
						lastObjects.add(new Tuple<Integer, CameraObject>(skip, obj));
					}
					break;
				case 6: // blue
					if(i + 1 == maxStoredObjects) { // latest update
						skip = 0;
						lastObjects.add(new Tuple<Integer, CameraObject>(skip, obj));
						notFound6 = false;
					} else if(notFound6) { // search the storage-list for older occurrences
						skip = (int)(latestCount - cObjectsList.get(i).key);
						lastObjects.add(new Tuple<Integer, CameraObject>(skip, obj));
						notFound6 = false;
					}
					break;
				}
			}
			
		}
		
		return lastObjects;
	}
	
	/**
	 * Draws CamaraRobots and EncoderRobots into the PCS.
	 * @param robot The Robot object to draw.
	 * @param svgRobot PShape robot image to draw.
	 */
	private void drawPCSRobot(PCSObject robot, PShape svgRobot) {
		pcsPGraphic.beginDraw();			
			pcsPGraphic.pushMatrix();
				pcsPGraphic.shapeMode(CENTER_MODE);
				pcsPGraphic.translate(pcsGSketchPadWidthHalf, pcsGSketchPadHeightHalf);
				pcsPGraphic.translate(convertXPositionToSketch(robot.getXPosition()), convertYPositionToSketch(robot.getYPosition()));
				pcsPGraphic.rotate(PApplet.radians(-(float)robot.getDirection()));
				pcsPGraphic.shape(svgRobot, 0, 0, robotDiameterPx, robotDiameterPx);
			pcsPGraphic.popMatrix();
		pcsPGraphic.endDraw();
	}

	/**
	 * Draws CameraObects into the PCS.
	 * @param cObject The CameraObject to draw.
	 * @param color The color used to draw the object (Processing data type).
	 */
	private void drawPCSObject(CameraObject cObject, int color) {
		pcsPGraphic.beginDraw();
			pcsPGraphic.pushMatrix();
				pcsPGraphic.fill(color);
				pcsPGraphic.translate(pcsGSketchPadWidthHalf, pcsGSketchPadHeightHalf);
				pcsPGraphic.translate(convertXPositionToSketch(cObject.getXPosition()), convertYPositionToSketch(cObject.getYPosition()));				
				pcsPGraphic.ellipse(0, 0, objectDiameterPx, objectDiameterPx);
			pcsPGraphic.popMatrix();
		pcsPGraphic.endDraw();
	}
	
	/**
	 * Draws an overlay image over CameraObjects in the pcs to indicate inactivity.
	 * @param cObject The CameraObject which holds the position where to draw the overlay
	 * @param svgOverlay The PShape image overlay to draw.
	 */
	private void drawPCSObjectOverlay(CameraObject cObject, PShape svgOverlay) {
		pcsPGraphic.beginDraw();			
		pcsPGraphic.pushMatrix();
			pcsPGraphic.shapeMode(CENTER_MODE);
			pcsPGraphic.translate(pcsGSketchPadWidthHalf, pcsGSketchPadHeightHalf);
			pcsPGraphic.translate(convertXPositionToSketch(cObject.getXPosition()), convertYPositionToSketch(cObject.getYPosition()));
			pcsPGraphic.rotate(PApplet.radians(-(float)cObject.getDirection()));
			pcsPGraphic.shape(svgOverlay, 0, 0, objectDiameterPx, objectDiameterPx);
		pcsPGraphic.popMatrix();
		pcsPGraphic.endDraw();
	}
	
	/**
	 * Updates the CameraRobot 1 text from the given object.
	 * @param robot The CameraRobot object which holds the data.
	 */
	private void updateCameraRobot1ControlText(PCSObject robot) {
		bot1NameGLabel.setText("1: " + robot.getName());
		bot1CameraXPosGLabel.setText("x   = " + robotXYFormat.format(robot.getXPosition()) + " cm");
		bot1CameraYPosGLabel.setText("y   = " + robotXYFormat.format(robot.getYPosition()) + " cm");
		bot1CameraDirectionGLabel.setText("phi = " + robotPhiFormat.format(robot.getDirection()) + " °");
	}
	
	/**
	 * Sets the color of the CameraRobot 1 text.
	 * @param color The color which is used to draw the text.
	 */
	public void setCameraRobot1ControlTextColor(int color) {
		if(color == -1) {
			bot1CameraXPosGLabel.setLocalColorScheme(COLOR_SCHEME);
			bot1CameraYPosGLabel.setLocalColorScheme(COLOR_SCHEME);
			bot1CameraDirectionGLabel.setLocalColorScheme(COLOR_SCHEME);
		} else {
			bot1CameraXPosGLabel.setLocalColor(2, color);
			bot1CameraYPosGLabel.setLocalColor(2, color);
			bot1CameraDirectionGLabel.setLocalColor(2, color);
		}
	}

	/**
	 * Updates the CameraRobot 2 text from the given object.
	 * @param robot The CameraRobot object which holds the data.
	 */

	private void updateCameraRobot2ControlText(PCSObject robot) {
		bot2NameGLabel.setText("2: " + robot.getName());
		bot2CameraXPosGLabel.setText("x   = " + robotXYFormat.format(robot.getXPosition()) + " cm");
		bot2CameraYPosGLabel.setText("y   = " + robotXYFormat.format(robot.getYPosition()) + " cm");
		bot2CameraDirectionGLabel.setText("phi = " + robotPhiFormat.format(robot.getDirection()) + " °");
	}
	
	/**
	 * Sets the color of the CameraRobot 2 text.
	 * @param color The color which is used to draw the text.
	 */
	public void setCameraRobot2ControlTextColor(int color) {
		if(color == -1) {
			bot2CameraXPosGLabel.setLocalColorScheme(COLOR_SCHEME);
			bot2CameraYPosGLabel.setLocalColorScheme(COLOR_SCHEME);
			bot2CameraDirectionGLabel.setLocalColorScheme(COLOR_SCHEME);
		} else {
			bot2CameraXPosGLabel.setLocalColor(2, color);
			bot2CameraYPosGLabel.setLocalColor(2, color);
			bot2CameraDirectionGLabel.setLocalColor(2, color);
		}
	}
	
	/**
	 * Updates the CameraRobot 3 text from the given object.
	 * @param robot The CameraRobot object which holds the data.
	 */
	private void updateCameraRobot3ControlText(PCSObject robot) {
		bot3NameGLabel.setText("3: " + robot.getName());
		bot3CameraXPosGLabel.setText("x   = " + robotXYFormat.format(robot.getXPosition()) + " cm");
		bot3CameraYPosGLabel.setText("y   = " + robotXYFormat.format(robot.getYPosition()) + " cm");
		bot3CameraDirectionGLabel.setText("phi = " + robotPhiFormat.format(robot.getDirection()) + " °");
	}
	
	/**
	 * Sets the color of the CameraRobot 3 text.
	 * @param color The color which is used to draw the text.
	 */
	public void setCameraRobot3ControlTextColor(int color) {
		if(color == -1) {
			bot3CameraXPosGLabel.setLocalColorScheme(COLOR_SCHEME);
			bot3CameraYPosGLabel.setLocalColorScheme(COLOR_SCHEME);
			bot3CameraDirectionGLabel.setLocalColorScheme(COLOR_SCHEME);
		} else {
			bot3CameraXPosGLabel.setLocalColor(2, color);
			bot3CameraYPosGLabel.setLocalColor(2, color);
			bot3CameraDirectionGLabel.setLocalColor(2, color);
		}
	}
	
	/**
	 * Updates the EncoderRobot 1 text from the given object.
	 * @param robot The EncoderRobot object which holds the data.
	 */
	private void updateEncoderRobot1ControlText(EncoderRobot robot) {
		bot1EncoderXPosGLabel.setText("x   = " + robotXYFormat.format(robot.getXPosition()) + " cm");
		bot1EncoderYPosGLabel.setText("y   = " + robotXYFormat.format(robot.getYPosition()) + " cm");
		bot1EncoderDirectionGLabel.setText("phi = " + robotPhiFormat.format(robot.getDirection()) + " °");
		bot1EncoderCBeamGLabel.setText(" Center    = " + robotdistanceFormat.format(robot.getBeamDistCenter()) + " cm");
		bot1EncoderLBeam45GLabel.setText(" Left-45°  = " + robotdistanceFormat.format(robot.getBeamDistLeft45()) + " cm");
		bot1EncoderRBeam45GLabel.setText(" Right-45° = " + robotdistanceFormat.format(robot.getBeamDistRight45()) + " cm");
	}

	/**
	 * Sets the color of the EncoderRobot 1 text.
	 * @param color The color which is used to draw the text.
	 */
	public void setEncoderRobot1ControlTextColor(int color) {
		if(color == -1) {
			bot1EncoderXPosGLabel.setLocalColorScheme(COLOR_SCHEME);
			bot1EncoderYPosGLabel.setLocalColorScheme(COLOR_SCHEME);
			bot1EncoderDirectionGLabel.setLocalColorScheme(COLOR_SCHEME);
			bot1EncoderDistanceGLabel.setLocalColorScheme(COLOR_SCHEME);
			bot1EncoderCBeamGLabel.setLocalColorScheme(COLOR_SCHEME);
			bot1EncoderLBeam45GLabel.setLocalColorScheme(COLOR_SCHEME);
			bot1EncoderRBeam45GLabel.setLocalColorScheme(COLOR_SCHEME);
		} else {
			bot1EncoderXPosGLabel.setLocalColor(2, color);
			bot1EncoderYPosGLabel.setLocalColor(2, color);
			bot1EncoderDirectionGLabel.setLocalColor(2, color);
			bot1EncoderDistanceGLabel.setLocalColor(2, color);
			bot1EncoderCBeamGLabel.setLocalColor(2, color);
			bot1EncoderLBeam45GLabel.setLocalColor(2, color);
			bot1EncoderRBeam45GLabel.setLocalColor(2, color);
		}
	}
	
	/**
	 * Updates the EncoderRobot 2 text from the given object.
	 * @param robot The EncoderRobot object which holds the data.
	 */
	private void updateEncoderRobot2ControlText(EncoderRobot robot) {
		bot2EncoderXPosGLabel.setText("x   = " + robotXYFormat.format(robot.getXPosition()) + " cm");
		bot2EncoderYPosGLabel.setText("y   = " + robotXYFormat.format(robot.getYPosition()) + " cm");
		bot2EncoderDirectionGLabel.setText("phi = " + robotPhiFormat.format(robot.getDirection()) + " °");
		bot2EncoderCBeamGLabel.setText(" Center    = " + robotdistanceFormat.format(robot.getBeamDistCenter()) + " cm");
		bot2EncoderLBeam45GLabel.setText(" Left-45°  = " + robotdistanceFormat.format(robot.getBeamDistLeft45()) + " cm");
		bot2EncoderRBeam45GLabel.setText(" Right-45° = " + robotdistanceFormat.format(robot.getBeamDistRight45()) + " cm");
	}

	/**
	 * Sets the color of the EncoderRobot 2 text.
	 * @param color The color which is used to draw the text.
	 */
	public void setEncoderRobot2ControlTextColor(int color) {
		if(color == -1) {
			bot2EncoderXPosGLabel.setLocalColorScheme(COLOR_SCHEME);
			bot2EncoderYPosGLabel.setLocalColorScheme(COLOR_SCHEME);
			bot2EncoderDirectionGLabel.setLocalColorScheme(COLOR_SCHEME);
			bot2EncoderDistanceGLabel.setLocalColorScheme(COLOR_SCHEME);
			bot2EncoderCBeamGLabel.setLocalColorScheme(COLOR_SCHEME);
			bot2EncoderLBeam45GLabel.setLocalColorScheme(COLOR_SCHEME);
			bot2EncoderRBeam45GLabel.setLocalColorScheme(COLOR_SCHEME);
		} else {
			bot2EncoderXPosGLabel.setLocalColor(2, color);
			bot2EncoderYPosGLabel.setLocalColor(2, color);
			bot2EncoderDirectionGLabel.setLocalColor(2, color);
			bot2EncoderDistanceGLabel.setLocalColor(2, color);
			bot2EncoderCBeamGLabel.setLocalColor(2, color);
			bot2EncoderLBeam45GLabel.setLocalColor(2, color);
			bot2EncoderRBeam45GLabel.setLocalColor(2, color);
		}
	}
	
	/**
	 * Updates the EncoderRobot 3 text from the given object.
	 * @param robot The EncoderRobot object which holds the data.
	 */
	private void updateEncoderRobot3ControlText(EncoderRobot robot) {
		bot3EncoderXPosGLabel.setText("x   = " + robotXYFormat.format(robot.getXPosition()) + " cm");
		bot3EncoderYPosGLabel.setText("y   = " + robotXYFormat.format(robot.getYPosition()) + " cm");
		bot3EncoderDirectionGLabel.setText("phi = " + robotPhiFormat.format(robot.getDirection()) + " °");
		bot3EncoderCBeamGLabel.setText(" Center    = " + robotdistanceFormat.format(robot.getBeamDistCenter()) + " cm");
		bot3EncoderLBeam45GLabel.setText(" Left-45°  = " + robotdistanceFormat.format(robot.getBeamDistLeft45()) + " cm");
		bot3EncoderRBeam45GLabel.setText(" Right-45° = " + robotdistanceFormat.format(robot.getBeamDistRight45()) + " cm");
	}

	/**
	 * Sets the color of the EncoderRobot 3 text.
	 * @param color The color which is used to draw the text.
	 */
	public void setEncoderRobot3ControlTextColor(int color) {
		if(color == -1) {
			bot3EncoderXPosGLabel.setLocalColorScheme(COLOR_SCHEME);
			bot3EncoderYPosGLabel.setLocalColorScheme(COLOR_SCHEME);
			bot3EncoderDirectionGLabel.setLocalColorScheme(COLOR_SCHEME);
			bot3EncoderDistanceGLabel.setLocalColorScheme(COLOR_SCHEME);
			bot3EncoderCBeamGLabel.setLocalColorScheme(COLOR_SCHEME);
			bot3EncoderLBeam45GLabel.setLocalColorScheme(COLOR_SCHEME);
			bot3EncoderRBeam45GLabel.setLocalColorScheme(COLOR_SCHEME);
		} else {
			bot3EncoderXPosGLabel.setLocalColor(2, color);
			bot3EncoderYPosGLabel.setLocalColor(2, color);
			bot3EncoderDirectionGLabel.setLocalColor(2, color);
			bot3EncoderDistanceGLabel.setLocalColor(2, color);
			bot3EncoderCBeamGLabel.setLocalColor(2, color);
			bot3EncoderLBeam45GLabel.setLocalColor(2, color);
			bot3EncoderRBeam45GLabel.setLocalColor(2, color);
		}
	}
	
	private void updateCameraObject1ControlText(CameraObject object, String color) {
		object1ColorGLabel.setText(" color = " + color);
		object1XPosGLabel.setText(" x   = " + objectXYFormat.format(object.getXPosition()) + " cm");
		object1YPosGLabel.setText(" y   = " + objectXYFormat.format(object.getYPosition()) + " cm");
	}
	
	public void setCameraObject1ControlTextColor(int color) {
		if(color == -1) {
			object1ColorGLabel.setLocalColorScheme(COLOR_SCHEME);
			object1XPosGLabel.setLocalColorScheme(COLOR_SCHEME);
			object1YPosGLabel.setLocalColorScheme(COLOR_SCHEME);
		} else {
			object1ColorGLabel.setLocalColor(2, color);
			object1XPosGLabel.setLocalColor(2, color);
			object1YPosGLabel.setLocalColor(2, color);
		}
	}
	
	private void updateCameraObject2ControlText(CameraObject object, String color) {
		object2ColorGLabel.setText(" color = " + color);
		object2XPosGLabel.setText(" x   = " + objectXYFormat.format(object.getXPosition()) + " cm");
		object2YPosGLabel.setText(" y   = " + objectXYFormat.format(object.getYPosition()) + " cm");
	}
	
	public void setCameraObject2ControlTextColor(int color) {
		if(color == -1) {
			object2ColorGLabel.setLocalColorScheme(COLOR_SCHEME);
			object2XPosGLabel.setLocalColorScheme(COLOR_SCHEME);
			object2YPosGLabel.setLocalColorScheme(COLOR_SCHEME);
		} else {
			object2ColorGLabel.setLocalColor(2, color);
			object2XPosGLabel.setLocalColor(2, color);
			object2YPosGLabel.setLocalColor(2, color);
		}
	}
	
	private void updateCameraObject3ControlText(CameraObject object, String color) {
		object3ColorGLabel.setText(" color = " + color);
		object3XPosGLabel.setText(" x   = " + objectXYFormat.format(object.getXPosition()) + " cm");
		object3YPosGLabel.setText(" y   = " + objectXYFormat.format(object.getYPosition()) + " cm");
	}
	
	public void setCameraObject3ControlTextColor(int color) {
		if(color == -1) {
			object3ColorGLabel.setLocalColorScheme(COLOR_SCHEME);
			object3XPosGLabel.setLocalColorScheme(COLOR_SCHEME);
			object3YPosGLabel.setLocalColorScheme(COLOR_SCHEME);
		} else {
			object3ColorGLabel.setLocalColor(2, color);
			object3XPosGLabel.setLocalColor(2, color);
			object3YPosGLabel.setLocalColor(2, color);
		}
	}
	
	private void updateCameraObject4ControlText(CameraObject object, String color) {
		object4ColorGLabel.setText(" color = " + color);
		object4XPosGLabel.setText(" x   = " + objectXYFormat.format(object.getXPosition()) + " cm");
		object4YPosGLabel.setText(" y   = " + objectXYFormat.format(object.getYPosition()) + " cm");
	}
	
	public void setCameraObject4ControlTextColor(int color) {
		if(color == -1) {
			object4ColorGLabel.setLocalColorScheme(COLOR_SCHEME);
			object4XPosGLabel.setLocalColorScheme(COLOR_SCHEME);
			object4YPosGLabel.setLocalColorScheme(COLOR_SCHEME);
		} else {
			object4ColorGLabel.setLocalColor(2, color);
			object4XPosGLabel.setLocalColor(2, color);
			object4YPosGLabel.setLocalColor(2, color);
		}
	}
	
	/**
	 * Converts the given x-position from the Model-data to the pcs-drawing in the Processing sketch.
	 * @param centeredPositionCm The given x-position in cm.
	 * @return The calculated relative x-position for drawing in the pcs-drawing.
	 */
	private int convertXPositionToSketch(double centeredPositionCm) {
		return (int)((centeredPositionCm * pcsGSketchPadWidthHalf) / mazeWidthHalfCm);
	}

	/**
	 * Converts the given y-position from the Model-data to the pcs-drawing in the Processing sketch.
	 * @param centeredPositionCm The given y-position in cm.
	 * @return The calculated relative y-position for drawing in the pcs-drawing.
	 */
	private int convertYPositionToSketch(double centeredPositionCm) {
		return (int)((-centeredPositionCm * pcsGSketchPadHeightHalf) / mazeHeightHalfCm);
	}

	/**
	 * Builds the pcs-drawing and all ui-elements needed for it.
	 */
	private void buildPCSSketchPad() {
		pcsGSketchPadHeight = (int)Math.round(parent.height - 2 * viewOuterBorder); 
		pcsGSketchPadWidth = pcsGSketchPadHeight;
		pcsGSketchPadWidthHalf = pcsGSketchPadWidth / 2;
		pcsGSketchPadHeightHalf = pcsGSketchPadHeight / 2;
		pcsGsketchPadXPosition = parent.width - pcsGSketchPadWidth - viewOuterBorder;
		pcsGsketchPadYPosition = (parent.height - pcsGSketchPadHeight) / 2;
		robotDiameterPx = (robotDiameterCm * pcsGSketchPadWidth / mazeWidthCm);
		objectDiameterPx = (objectDiameterCm * pcsGSketchPadWidth / mazeWidthCm);
		xPosLaserOrigin = robotDiameterPx / 4;

		// Create the G4P control to position and display the graphic
		pcsGSketchPad = new GSketchPad(parent, pcsGsketchPadXPosition, pcsGsketchPadYPosition, pcsGSketchPadWidth, pcsGSketchPadHeight);
		// Create a PGraphic with the G4P control dimensions
		pcsPGraphic = parent.createGraphics(pcsGSketchPadWidth, pcsGSketchPadHeight);
		// Set the graphic for this control. The graphic will be scaled to fit the control.
		pcsGSketchPad.setGraphic(pcsPGraphic);

		svgBlueBird = pcsPGraphic.loadShape("blue_bird.svg");
		svgGreenFalcon = pcsPGraphic.loadShape("green_falcon.svg");
		svgRedDragon = pcsPGraphic.loadShape("red_dragon.svg");
		svgDefault = pcsPGraphic.loadShape("default.svg");
		svgEncoderRobot1 = pcsPGraphic.loadShape("e1.svg");
		svgEncoderRobot2 = pcsPGraphic.loadShape("e2.svg");
		svgEncoderRobot3 = pcsPGraphic.loadShape("e3.svg");
		svgInactiveOverlay = pcsPGraphic.loadShape("svgInactiveOverlay.svg");
	}
	
	/**
	 * Builds the control-panel which displays the text for robots and objects and all ui-elements needed for it.
	 */
	private void buildControlPanel() {
		controlGSketchPadWidth = parent.width - pcsGSketchPadWidth - 3 * viewOuterBorder;
		controlGSketchPadHeight = parent.height - 2 * viewOuterBorder;
		controlGPanelXPosition = viewOuterBorder;
		controlGPanelYPosition = viewOuterBorder;
		controlSpacing = (int)Math.round(controlGSketchPadWidth / 35.0);
		controlSpacingHalf = (int)Math.round(controlSpacing / 2);
		controlColumnStandardHeight = 16;
		controlColumnHeadingHeight = 20;
		controlTextWidth = controlGSketchPadWidth - 2 * controlSpacing;
		controlColumn2Width = (int)Math.round((controlGSketchPadWidth - 4 * controlSpacing) / 2.7d);
		controlColumn3Width = controlColumn2Width;
		controlColumn1Width = controlGSketchPadWidth - 4 * controlSpacing - 2 * controlColumn2Width;		
		
		controlContainerGPanel = new GPanel(parent, controlGPanelXPosition, controlGPanelYPosition, controlGSketchPadWidth, controlGSketchPadHeight, "Control Panel");
		controlContainerGPanel.setCollapsed(false);
		controlContainerGPanel.setCollapsible(false);
		controlContainerGPanel.setDraggable(false);
		controlContainerGPanel.setLocalColorScheme(COLOR_SCHEME, false);
		column1BotXPos = controlSpacing;
		column2BotXPos = column1BotXPos + controlColumn1Width + controlSpacing;
		column3BotXPos = column2BotXPos + controlColumn2Width + controlSpacing;
		controlYPos = controlSpacing + controlContainerGPanel.getTabHeight();
		controlBotRectYPos = controlYPos;
		
		controlPGraphic = parent.createGraphics(controlGSketchPadWidth, pcsGSketchPadHeight);
		controlGSketchPad = new GSketchPad(parent, 0, 0, controlGSketchPadWidth, controlGSketchPadHeight);
		controlGSketchPad.setGraphic(controlPGraphic);
		controlContainerGPanel.addControl(controlGSketchPad);
		controlsFont = FontManager.getPriorityFont(monoSpaced, Font.BOLD, 13); 
		buildCameraRobotHeadings();
		controlsFont = FontManager.getPriorityFont(monoSpaced, Font.PLAIN, 10);
		buildRobot1Text();
		buildCameraRobot2Text();
		buildCameraRobot3Text();
		buildObjectText();
		buildOptions();
		
	}
	
	/**
	 * Builds the heading text ui-elements for URGino 1-3.
	 */
	private void buildCameraRobotHeadings() {
		column1Heading = new GLabel(parent, 0, 0, controlColumn1Width, controlColumnHeadingHeight);
		buildGTextIconElement(column1Heading, "URGino");
		column2Heading = new GLabel(parent, 0, 0, controlColumn2Width, controlColumnHeadingHeight);
		buildGTextIconElement(column2Heading, "Ext. Camera Data");
		column3Heading = new GLabel(parent, 0, 0, controlColumn3Width, controlColumnHeadingHeight);
		buildGTextIconElement(column3Heading, "Int. Bot-Odometry Data");
		controlContainerGPanel.addControl(column1Heading, column1BotXPos, controlYPos);
		controlContainerGPanel.addControl(column2Heading, column2BotXPos, controlYPos);
		controlContainerGPanel.addControl(column3Heading, column3BotXPos, controlYPos);
		// draw line
		controlPGraphic.beginDraw();
			controlPGraphic.stroke(150);
			controlPGraphic.strokeWeight(1);
			controlPGraphic.line(column1BotXPos, controlYPos + controlColumnHeadingHeight, column1BotXPos + controlTextWidth, controlYPos + controlColumnHeadingHeight);
		controlPGraphic.endDraw();
	}
	
	/**
	 * Builds the text ui-elements for URGino 1.
	 */
	private void buildRobot1Text() {
		controlYPos += controlColumnHeadingHeight + controlSpacingHalf;
		// draw line
		controlPGraphic.beginDraw();
			controlPGraphic.stroke(150);
			controlPGraphic.strokeWeight(1);
			controlPGraphic.line(column1BotXPos, controlYPos, column1BotXPos + controlTextWidth, controlYPos);
		controlPGraphic.endDraw();
		// acaBot 1 line 1
		bot1NameGLabel = new GLabel(parent, 0, 0, controlColumn1Width, controlColumnStandardHeight);
		buildGTextIconElement(bot1NameGLabel, "1: " + ROBOT_RED_DRAGON);
		bot1CameraXPosGLabel = new GLabel(parent, 0, 0, controlColumn2Width, controlColumnStandardHeight);
		buildGTextIconElement(bot1CameraXPosGLabel, "x   = 0.00 cm");
		bot1EncoderXPosGLabel = new GLabel(parent, 0, 0, controlColumn3Width, controlColumnStandardHeight);
		buildGTextIconElement(bot1EncoderXPosGLabel, "x   = 0.00 cm");
		controlContainerGPanel.addControl(bot1NameGLabel, column1BotXPos, controlYPos);
		controlContainerGPanel.addControl(bot1CameraXPosGLabel, column2BotXPos, controlYPos);
		controlContainerGPanel.addControl(bot1EncoderXPosGLabel, column3BotXPos, controlYPos);
		// acaBot 1 line 2
		bot1CameraYPosGLabel = new GLabel(parent, 0, 0, controlColumn2Width, controlColumnStandardHeight);
		buildGTextIconElement(bot1CameraYPosGLabel, "y   = 0.00 cm");
		bot1EncoderYPosGLabel = new GLabel(parent, 0, 0, controlColumn3Width, controlColumnStandardHeight);
		buildGTextIconElement(bot1EncoderYPosGLabel, "y   = 0.00 cm");
		controlYPos += controlColumnStandardHeight;
		controlContainerGPanel.addControl(bot1CameraYPosGLabel, column2BotXPos, controlYPos);
		controlContainerGPanel.addControl(bot1EncoderYPosGLabel, column3BotXPos, controlYPos);
		// acaBot 1 line 3
		bot1IconGImageButton = new GImageButton(parent, 0, 0, 3 * controlColumnStandardHeight, 3 * controlColumnStandardHeight, new String[] {"red_dragon.png"});
		bot1IconGImageButton.setEnabled(false);
		bot1ShowOptionsGLabel = new GLabel(parent, 0, 0, controlColumn1Width - bot1IconGImageButton.getWidth() - controlSpacingHalf, controlColumnStandardHeight);
		buildGTextIconElement(bot1ShowOptionsGLabel, "Show:");
		bot1CameraDirectionGLabel = new GLabel(parent, 0, 0, controlColumn2Width, controlColumnStandardHeight);
		buildGTextIconElement(bot1CameraDirectionGLabel, "phi = 0.00 °");
		bot1EncoderDirectionGLabel = new GLabel(parent, 0, 0, controlColumn3Width, controlColumnStandardHeight);
		buildGTextIconElement(bot1EncoderDirectionGLabel, "phi = 0.00 °");
		controlYPos += controlColumnStandardHeight;
		controlContainerGPanel.addControl(bot1ShowOptionsGLabel, column1BotXPos + bot1IconGImageButton.getWidth() + controlSpacingHalf, controlYPos);
		controlContainerGPanel.addControl(bot1IconGImageButton, column1BotXPos + controlSpacingHalf / 2, controlYPos);
		controlContainerGPanel.addControl(bot1CameraDirectionGLabel, column2BotXPos, controlYPos);
		controlContainerGPanel.addControl(bot1EncoderDirectionGLabel, column3BotXPos, controlYPos);
		// acaBot 1 line 4		
		showBot1CameraGCheckbox = new GCheckbox(parent, 0, 0, controlColumn1Width - bot1IconGImageButton.getWidth() - controlSpacingHalf, controlColumnStandardHeight);
		buildGTextIconElement(showBot1CameraGCheckbox, "Camera");
		bot1EncoderDistanceGLabel = new GLabel(parent, 0, 0, controlColumn3Width, controlColumnStandardHeight);
		buildGTextIconElement(bot1EncoderDistanceGLabel, "Distance Sensor:");
		controlYPos += controlColumnStandardHeight;
		controlContainerGPanel.addControl(showBot1CameraGCheckbox, column1BotXPos + bot1IconGImageButton.getWidth() + controlSpacingHalf, controlYPos);
		controlContainerGPanel.addControl(bot1EncoderDistanceGLabel, column3BotXPos, controlYPos);
		// acaBot1 line 5
		showBot1EncoderGCheckbox = new GCheckbox(parent, 0, 0, controlColumn1Width - bot1IconGImageButton.getWidth() - controlSpacingHalf, controlColumnStandardHeight);
		buildGTextIconElement(showBot1EncoderGCheckbox, "Odometry");
		bot1EncoderCBeamGLabel = new GLabel(parent, 0, 0, controlColumn3Width, controlColumnStandardHeight);
		buildGTextIconElement(bot1EncoderCBeamGLabel, " Center    = 0.00 cm");
		controlYPos += controlColumnStandardHeight;
		controlContainerGPanel.addControl(showBot1EncoderGCheckbox, column1BotXPos + bot1IconGImageButton.getWidth() + controlSpacingHalf, controlYPos);
		controlContainerGPanel.addControl(bot1EncoderCBeamGLabel, column3BotXPos, controlYPos);
		// acaBot 1 line 6
		showBot1LaserGCheckbox = new GCheckbox(parent, 0, 0, controlColumn1Width - bot1IconGImageButton.getWidth() - controlSpacingHalf, controlColumnStandardHeight);
		buildGTextIconElement(showBot1LaserGCheckbox, "Laser");
		bot1EncoderLBeam45GLabel = new GLabel(parent, 0, 0, controlColumn3Width, controlColumnStandardHeight);
		buildGTextIconElement(bot1EncoderLBeam45GLabel, " Left-45°  = 0.00 cm");
		controlYPos += controlColumnStandardHeight;
		controlContainerGPanel.addControl(showBot1LaserGCheckbox, column1BotXPos + (float)(bot1IconGImageButton.getWidth() * 1.2) + controlSpacingHalf, controlYPos);
		controlContainerGPanel.addControl(bot1EncoderLBeam45GLabel, column3BotXPos, controlYPos);
		// acaBot 1 line  7
		bot1EncoderRBeam45GLabel = new GLabel(parent, 0, 0, controlColumn3Width, controlColumnStandardHeight);
		buildGTextIconElement(bot1EncoderRBeam45GLabel, " Right-45° = 0.00 cm");
		controlYPos += controlColumnStandardHeight;
		controlContainerGPanel.addControl(bot1EncoderRBeam45GLabel, column3BotXPos, controlYPos);
		
		setCameraRobot1ControlTextColor(GREY);
		setEncoderRobot1ControlTextColor(GREY);
		showBot1CameraGCheckbox.setSelected(true);
	}

	/**
	 * Builds the text ui-elements for URGino 2.
	 */
	private void buildCameraRobot2Text() {
		controlYPos += controlColumnStandardHeight;
		// draw line
		controlPGraphic.beginDraw();
			controlPGraphic.stroke(150);
			controlPGraphic.strokeWeight(1);
			controlPGraphic.line(column1BotXPos, controlYPos, column1BotXPos + controlTextWidth, controlYPos);
		controlPGraphic.endDraw();
		// acaBot 2 line 1		
		bot2NameGLabel = new GLabel(parent, 0, 0, controlColumn1Width, controlColumnStandardHeight);
		buildGTextIconElement(bot2NameGLabel, "2: " + ROBOT_BLUE_BIRD);
		bot2CameraXPosGLabel = new GLabel(parent, 0, 0, controlColumn2Width, controlColumnStandardHeight);
		buildGTextIconElement(bot2CameraXPosGLabel, "x   = 0.00 cm");
		bot2EncoderXPosGLabel = new GLabel(parent, 0, 0, controlColumn3Width, controlColumnStandardHeight);
		buildGTextIconElement(bot2EncoderXPosGLabel, "x   = 0.00 cm");
		controlContainerGPanel.addControl(bot2NameGLabel, column1BotXPos, controlYPos);
		controlContainerGPanel.addControl(bot2CameraXPosGLabel, column2BotXPos, controlYPos);	
		controlContainerGPanel.addControl(bot2EncoderXPosGLabel, column3BotXPos, controlYPos);
		// acaBot 2 line 2
		bot2CameraYPosGLabel = new GLabel(parent, 0, 0, controlColumn2Width, controlColumnStandardHeight);
		buildGTextIconElement(bot2CameraYPosGLabel, "y   = 0.00 cm");
		bot2EncoderYPosGLabel = new GLabel(parent, 0, 0, controlColumn3Width, controlColumnStandardHeight);
		buildGTextIconElement(bot2EncoderYPosGLabel, "y   = 0.00 cm");
		controlYPos += controlColumnStandardHeight;
		controlContainerGPanel.addControl(bot2CameraYPosGLabel, column2BotXPos, controlYPos);
		controlContainerGPanel.addControl(bot2EncoderYPosGLabel, column3BotXPos, controlYPos);
		// acaBot 2 line 3
		bot2IconGImageButton = new GImageButton(parent, 0, 0, 3 * controlColumnStandardHeight, 3 * controlColumnStandardHeight, new String[] {"blue_bird.png"});
		bot2IconGImageButton.setEnabled(false);
		bot2ShowOptionsGLabel = new GLabel(parent, 0, 0, controlColumn1Width - bot2IconGImageButton.getWidth() - controlSpacingHalf, controlColumnStandardHeight);
		buildGTextIconElement(bot2ShowOptionsGLabel, "Show:");		bot2CameraDirectionGLabel = new GLabel(parent, 0, 0, controlColumn2Width, controlColumnStandardHeight);
		buildGTextIconElement(bot2CameraDirectionGLabel, "phi = 0.00 °");
		bot2EncoderDirectionGLabel = new GLabel(parent, 0, 0, controlColumn3Width, controlColumnStandardHeight);
		buildGTextIconElement(bot2EncoderDirectionGLabel, "phi = 0.00 °");
		controlYPos += controlColumnStandardHeight;
		controlContainerGPanel.addControl(bot2ShowOptionsGLabel, column1BotXPos + bot1IconGImageButton.getWidth() + controlSpacingHalf, controlYPos);
		controlContainerGPanel.addControl(bot2IconGImageButton, column1BotXPos + controlSpacingHalf / 2, controlYPos);
		controlContainerGPanel.addControl(bot2CameraDirectionGLabel, column2BotXPos, controlYPos);	
		controlContainerGPanel.addControl(bot2EncoderDirectionGLabel, column3BotXPos, controlYPos);
		// acaBot 2 line 4		
		showBot2CameraGCheckbox = new GCheckbox(parent, 0, 0, controlColumn1Width - bot1IconGImageButton.getWidth() - controlSpacingHalf, controlColumnStandardHeight);
		buildGTextIconElement(showBot2CameraGCheckbox, "Camera");
		bot2EncoderDistanceGLabel = new GLabel(parent, 0, 0, controlColumn3Width, controlColumnStandardHeight);
		buildGTextIconElement(bot2EncoderDistanceGLabel, "Distance Sensor:");
		controlYPos += controlColumnStandardHeight;
		controlContainerGPanel.addControl(showBot2CameraGCheckbox, column1BotXPos + bot1IconGImageButton.getWidth() + controlSpacingHalf, controlYPos);
		controlContainerGPanel.addControl(bot2EncoderDistanceGLabel, column3BotXPos, controlYPos);
		// acaBot 2 line 5
		showBot2EncoderGCheckbox = new GCheckbox(parent, 0, 0, controlColumn1Width - bot1IconGImageButton.getWidth() - controlSpacingHalf, controlColumnStandardHeight);
		buildGTextIconElement(showBot2EncoderGCheckbox, "Odometry");
		bot2EncoderCBeamGLabel = new GLabel(parent, 0, 0, controlColumn3Width, controlColumnStandardHeight);
		buildGTextIconElement(bot2EncoderCBeamGLabel, " Center    = 0.00 cm");
		controlYPos += controlColumnStandardHeight;
		controlContainerGPanel.addControl(showBot2EncoderGCheckbox, column1BotXPos + bot1IconGImageButton.getWidth() + controlSpacingHalf, controlYPos);
		controlContainerGPanel.addControl(bot2EncoderCBeamGLabel, column3BotXPos, controlYPos);
		// acaBot 2 line 6
		showBot2LaserGCheckbox = new GCheckbox(parent, 0, 0, controlColumn1Width - bot1IconGImageButton.getWidth() - controlSpacingHalf, controlColumnStandardHeight);
		buildGTextIconElement(showBot2LaserGCheckbox, "Laser");
		bot2EncoderLBeam45GLabel = new GLabel(parent, 0, 0, controlColumn3Width, controlColumnStandardHeight);
		buildGTextIconElement(bot2EncoderLBeam45GLabel, " Left-45°  = 0.00 cm");
		controlYPos += controlColumnStandardHeight;
		controlContainerGPanel.addControl(showBot2LaserGCheckbox, column1BotXPos + (float)(bot1IconGImageButton.getWidth() * 1.2) + controlSpacingHalf, controlYPos);
		controlContainerGPanel.addControl(bot2EncoderLBeam45GLabel, column3BotXPos, controlYPos);
		// acaBot 2 line  7
		bot2EncoderRBeam45GLabel = new GLabel(parent, 0, 0, controlColumn3Width, controlColumnStandardHeight);
		buildGTextIconElement(bot2EncoderRBeam45GLabel, " Right-45° = 0.00 cm");
		controlYPos += controlColumnStandardHeight;
		controlContainerGPanel.addControl(bot2EncoderRBeam45GLabel, column3BotXPos, controlYPos);
		
		setCameraRobot2ControlTextColor(GREY);
		setEncoderRobot2ControlTextColor(GREY);
		showBot2CameraGCheckbox.setSelected(true);
	}
	
	/**
	 * Builds the text ui-elements for URGino 3.
	 */
	private void buildCameraRobot3Text() {
		controlYPos += controlColumnStandardHeight;
		// draw line
		controlPGraphic.beginDraw();
			controlPGraphic.stroke(150);
			controlPGraphic.strokeWeight(1);
			controlPGraphic.line(column1BotXPos, controlYPos, column1BotXPos + controlTextWidth, controlYPos);
		controlPGraphic.endDraw();
		// acaBot 3 line 1		
		bot3NameGLabel = new GLabel(parent, 0, 0, controlColumn1Width, controlColumnStandardHeight);
		buildGTextIconElement(bot3NameGLabel, "3: " + ROBOT_GREEN_FALCON);
		bot3CameraXPosGLabel = new GLabel(parent, 0, 0, controlColumn2Width, controlColumnStandardHeight);
		buildGTextIconElement(bot3CameraXPosGLabel, "x   = 0.00 cm");
		bot3EncoderXPosGLabel = new GLabel(parent, 0, 0, controlColumn3Width, controlColumnStandardHeight);
		buildGTextIconElement(bot3EncoderXPosGLabel, "x   = 0.00 cm");
		controlContainerGPanel.addControl(bot3NameGLabel, column1BotXPos, controlYPos);
		controlContainerGPanel.addControl(bot3CameraXPosGLabel, column2BotXPos, controlYPos);
		controlContainerGPanel.addControl(bot3EncoderXPosGLabel, column3BotXPos, controlYPos);
		// acaBot 3 line 2
		bot3CameraYPosGLabel = new GLabel(parent, 0, 0, controlColumn2Width, controlColumnStandardHeight);
		buildGTextIconElement(bot3CameraYPosGLabel, "y   = 0.00 cm");
		bot3EncoderYPosGLabel = new GLabel(parent, 0, 0, controlColumn3Width, controlColumnStandardHeight);
		buildGTextIconElement(bot3EncoderYPosGLabel, "y   = 0.00 cm");
		controlYPos += controlColumnStandardHeight;
		controlContainerGPanel.addControl(bot3CameraYPosGLabel, column2BotXPos, controlYPos);
		controlContainerGPanel.addControl(bot3EncoderYPosGLabel, column3BotXPos, controlYPos);
		// acaBot 3 line 3
		bot3IconGImageButton = new GImageButton(parent, 0, 0, 3 * controlColumnStandardHeight, 3 * controlColumnStandardHeight, new String[] {"green_falcon.png"});
		bot3IconGImageButton.setEnabled(false);
		bot3ShowOptionsGLabel = new GLabel(parent, 0, 0, controlColumn1Width - bot3IconGImageButton.getWidth() - controlSpacingHalf, controlColumnStandardHeight);
		buildGTextIconElement(bot3ShowOptionsGLabel, "Show:");		bot3CameraDirectionGLabel = new GLabel(parent, 0, 0, controlColumn2Width, controlColumnStandardHeight);
		buildGTextIconElement(bot3CameraDirectionGLabel, "phi = 0.00 °");
		bot3EncoderDirectionGLabel = new GLabel(parent, 0, 0, controlColumn3Width, controlColumnStandardHeight);
		buildGTextIconElement(bot3EncoderDirectionGLabel, "phi = 0.00 °");
		controlYPos += controlColumnStandardHeight;
		controlContainerGPanel.addControl(bot3ShowOptionsGLabel, column1BotXPos + bot3IconGImageButton.getWidth() + controlSpacingHalf, controlYPos);
		controlContainerGPanel.addControl(bot3IconGImageButton, column1BotXPos + controlSpacingHalf / 2, controlYPos);
		controlContainerGPanel.addControl(bot3CameraDirectionGLabel, column2BotXPos, controlYPos);
		controlContainerGPanel.addControl(bot3EncoderDirectionGLabel, column3BotXPos, controlYPos);
		// acaBot 3 line 4		
		showBot3CameraGCheckbox = new GCheckbox(parent, 0, 0, controlColumn1Width - bot1IconGImageButton.getWidth() - controlSpacingHalf, controlColumnStandardHeight);
		buildGTextIconElement(showBot3CameraGCheckbox, "Camera");
		bot3EncoderDistanceGLabel = new GLabel(parent, 0, 0, controlColumn3Width, controlColumnStandardHeight);
		buildGTextIconElement(bot3EncoderDistanceGLabel, "Distance Sensor:");
		controlYPos += controlColumnStandardHeight;
		controlContainerGPanel.addControl(showBot3CameraGCheckbox, column1BotXPos + bot1IconGImageButton.getWidth() + controlSpacingHalf, controlYPos);
		controlContainerGPanel.addControl(bot3EncoderDistanceGLabel, column3BotXPos, controlYPos);
		// acaBot 3 line 5
		showBot3EncoderGCheckbox = new GCheckbox(parent, 0, 0, controlColumn1Width - bot1IconGImageButton.getWidth() - controlSpacingHalf, controlColumnStandardHeight);
		buildGTextIconElement(showBot3EncoderGCheckbox, "Odometry");
		bot3EncoderCBeamGLabel = new GLabel(parent, 0, 0, controlColumn3Width, controlColumnStandardHeight);
		buildGTextIconElement(bot3EncoderCBeamGLabel, " Center    = 0.00 cm");
		controlYPos += controlColumnStandardHeight;
		controlContainerGPanel.addControl(showBot3EncoderGCheckbox, column1BotXPos + bot1IconGImageButton.getWidth() + controlSpacingHalf, controlYPos);
		controlContainerGPanel.addControl(bot3EncoderCBeamGLabel, column3BotXPos, controlYPos);
		// acaBot 3 line 6
		showBot3LaserGCheckbox = new GCheckbox(parent, 0, 0, controlColumn1Width - bot1IconGImageButton.getWidth() - controlSpacingHalf, controlColumnStandardHeight);
		buildGTextIconElement(showBot3LaserGCheckbox, "Laser");
		bot3EncoderLBeam45GLabel = new GLabel(parent, 0, 0, controlColumn3Width, controlColumnStandardHeight);
		buildGTextIconElement(bot3EncoderLBeam45GLabel, " Left-45°  = 0.00 cm");
		controlYPos += controlColumnStandardHeight;
		controlContainerGPanel.addControl(showBot3LaserGCheckbox, column1BotXPos + (float)(bot1IconGImageButton.getWidth() * 1.2) + controlSpacingHalf, controlYPos);
		controlContainerGPanel.addControl(bot3EncoderLBeam45GLabel, column3BotXPos, controlYPos);
		// acaBot 3 line  7
		bot3EncoderRBeam45GLabel = new GLabel(parent, 0, 0, controlColumn3Width, controlColumnStandardHeight);
		buildGTextIconElement(bot3EncoderRBeam45GLabel, " Right-45° = 0.00 cm");
		controlYPos += controlColumnStandardHeight;
		controlContainerGPanel.addControl(bot3EncoderRBeam45GLabel, column3BotXPos, controlYPos);
		// draw rectangle around all bots data - including headings
		controlPGraphic.beginDraw();
			controlPGraphic.noFill();
			controlPGraphic.stroke(150);
			controlPGraphic.strokeWeight(1);
			controlPGraphic.rect(column1BotXPos, controlBotRectYPos, controlTextWidth, controlYPos - controlSpacing);
		controlPGraphic.endDraw();
		
		setCameraRobot3ControlTextColor(GREY);
		setEncoderRobot3ControlTextColor(GREY);
		showBot3CameraGCheckbox.setSelected(true);
	}
	
	/**
	 * Builds the text ui-elements for objects.
	 */
	private void buildObjectText() {
		columnObjectWidth = Math.round(controlTextWidth / 5);
		column1ObjectXPos = controlSpacing;
		column2ObjectXPos = column1ObjectXPos + columnObjectWidth;
		column3ObjectXPos = column2ObjectXPos + columnObjectWidth;
		column4ObjectXPos = column3ObjectXPos + columnObjectWidth;
		column5ObjectXPos = column4ObjectXPos + columnObjectWidth;
		// draw rectangle
		controlYPos += (controlColumnStandardHeight + controlSpacingHalf);
		controlPGraphic.beginDraw();
			controlPGraphic.noFill();
			controlPGraphic.stroke(150);
			controlPGraphic.strokeWeight(1);
			controlPGraphic.rect(column1BotXPos, controlYPos, controlTextWidth, 4 * controlColumnStandardHeight);
		controlPGraphic.endDraw();	
		// Objects line 1
		showObjectsGCheckbox = new GCheckbox(parent, 0, 0, columnObjectWidth, controlColumnStandardHeight);
		buildGTextIconElement(showObjectsGCheckbox, "Show Objects");
		object1GLabel = new GLabel(parent, 0, 0, columnObjectWidth, controlColumnStandardHeight);
		buildGTextIconElement(object1GLabel, "Object 1:");
		object2GLabel = new GLabel(parent, 0, 0, columnObjectWidth, controlColumnStandardHeight);
		buildGTextIconElement(object2GLabel, "Object 2:");
		object3GLabel = new GLabel(parent, 0, 0, columnObjectWidth, controlColumnStandardHeight);
		buildGTextIconElement(object3GLabel, "Object 3:");
		object4GLabel = new GLabel(parent, 0, 0, columnObjectWidth, controlColumnStandardHeight);
		buildGTextIconElement(object4GLabel, "Object 4:");
		controlContainerGPanel.addControl(showObjectsGCheckbox, column1ObjectXPos, controlYPos);
		controlContainerGPanel.addControl(object1GLabel, column2ObjectXPos, controlYPos);
		controlContainerGPanel.addControl(object2GLabel, column3ObjectXPos, controlYPos);
		controlContainerGPanel.addControl(object3GLabel, column4ObjectXPos, controlYPos);
		controlContainerGPanel.addControl(object4GLabel, column5ObjectXPos, controlYPos);
		// Objects line 2
		object1ColorGLabel = new GLabel(parent, 0, 0, columnObjectWidth, controlColumnStandardHeight);
		buildGTextIconElement(object1ColorGLabel, " color = RED");
		object2ColorGLabel = new GLabel(parent, 0, 0, columnObjectWidth, controlColumnStandardHeight);
		buildGTextIconElement(object2ColorGLabel, " color = YELLOW");
		object3ColorGLabel = new GLabel(parent, 0, 0, columnObjectWidth, controlColumnStandardHeight);
		buildGTextIconElement(object3ColorGLabel, " color = GREEN");
		object4ColorGLabel = new GLabel(parent, 0, 0, columnObjectWidth, controlColumnStandardHeight);
		buildGTextIconElement(object4ColorGLabel, " color = BLUE");
		controlYPos += controlColumnStandardHeight;
		controlContainerGPanel.addControl(object1ColorGLabel, column2ObjectXPos, controlYPos);
		controlContainerGPanel.addControl(object2ColorGLabel, column3ObjectXPos, controlYPos);
		controlContainerGPanel.addControl(object3ColorGLabel, column4ObjectXPos, controlYPos);
		controlContainerGPanel.addControl(object4ColorGLabel, column5ObjectXPos, controlYPos);
		// Objects line 3
		object1XPosGLabel = new GLabel(parent, 0, 0, columnObjectWidth, controlColumnStandardHeight);
		buildGTextIconElement(object1XPosGLabel, " x     = 0.00");
		object2XPosGLabel = new GLabel(parent, 0, 0, columnObjectWidth, controlColumnStandardHeight);
		buildGTextIconElement(object2XPosGLabel, " x     = 0.00");
		object3XPosGLabel = new GLabel(parent, 0, 0, columnObjectWidth, controlColumnStandardHeight);
		buildGTextIconElement(object3XPosGLabel, " x     = 0.00");
		object4XPosGLabel = new GLabel(parent, 0, 0, columnObjectWidth, controlColumnStandardHeight);
		buildGTextIconElement(object4XPosGLabel, " x     = 0.00");
		controlYPos += controlColumnStandardHeight;
		controlContainerGPanel.addControl(object1XPosGLabel, column2ObjectXPos, controlYPos);
		controlContainerGPanel.addControl(object2XPosGLabel, column3ObjectXPos, controlYPos);
		controlContainerGPanel.addControl(object3XPosGLabel, column4ObjectXPos, controlYPos);
		controlContainerGPanel.addControl(object4XPosGLabel, column5ObjectXPos, controlYPos);
		// Objects line 4
		object1YPosGLabel = new GLabel(parent, 0, 0, columnObjectWidth, controlColumnStandardHeight);
		buildGTextIconElement(object1YPosGLabel, " y     = 0.00");
		object2YPosGLabel = new GLabel(parent, 0, 0, columnObjectWidth, controlColumnStandardHeight);
		buildGTextIconElement(object2YPosGLabel, " y     = 0.00");
		object3YPosGLabel = new GLabel(parent, 0, 0, columnObjectWidth, controlColumnStandardHeight);
		buildGTextIconElement(object3YPosGLabel, " y     = 0.00");
		object4YPosGLabel = new GLabel(parent, 0, 0, columnObjectWidth, controlColumnStandardHeight);
		buildGTextIconElement(object4YPosGLabel, " y     = 0.00");
		controlYPos += controlColumnStandardHeight;
		controlContainerGPanel.addControl(object1YPosGLabel, column2ObjectXPos, controlYPos);
		controlContainerGPanel.addControl(object2YPosGLabel, column3ObjectXPos, controlYPos);
		controlContainerGPanel.addControl(object3YPosGLabel, column4ObjectXPos, controlYPos);
		controlContainerGPanel.addControl(object4YPosGLabel, column5ObjectXPos, controlYPos);
		
		setCameraObject1ControlTextColor(GREY);
		setCameraObject2ControlTextColor(GREY);
		setCameraObject3ControlTextColor(GREY);
		setCameraObject4ControlTextColor(GREY);
		showObjectsGCheckbox.setSelected(true);
	}
	
	/**
	 * Builds the ui-elements used for drawing the options in the control-panel.
	 */
	private void buildOptions() {
		columnOptionsWidth = Math.round(controlTextWidth / 3);
		column1OptionsXPos = controlSpacing;
		column2OptionsXPos = column1OptionsXPos + columnOptionsWidth;
		column3OptionsXPos = column2OptionsXPos + columnOptionsWidth;
		columnOptionsWidthEff = columnOptionsWidth - 2 * controlSpacing;
		// draw rectangle around all Options
		controlYPos += controlColumnStandardHeight + controlSpacingHalf;
		controlPGraphic.beginDraw();
			controlPGraphic.noFill();
			controlPGraphic.stroke(150);
			controlPGraphic.strokeWeight(1);
			controlPGraphic.rect(column1BotXPos, controlYPos, controlTextWidth, controlGSketchPadHeight - controlYPos - controlSpacing);
		controlPGraphic.endDraw();

		//draw options column 1: Update acaBots
		controlsFont = FontManager.getPriorityFont(monoSpaced, Font.BOLD, 14);
		updateHeadingGLabel = new GLabel(parent, 0, 0, columnOptionsWidthEff, controlColumnHeadingHeight);
		updateHeadingGLabel.setTextAlign(GAlign.LEFT, null);
		buildGTextIconElement(updateHeadingGLabel, "Update Source:");

		controlsFont = FontManager.getPriorityFont(monoSpaced, Font.PLAIN, 10);
		int controlC1YPos = controlYPos + controlSpacing;
		controlContainerGPanel.addControl(updateHeadingGLabel, column1OptionsXPos + controlSpacing, controlC1YPos);
	
		updateCameraDataCheckbox = new GCheckbox(parent, 0, 0, columnOptionsWidthEff, controlColumnStandardHeight);
		updateCameraDataCheckbox.setSelected(true);
		buildGTextIconElement(updateCameraDataCheckbox, "Camera Data");
		controlC1YPos +=  updateHeadingGLabel.getHeight() + controlSpacingHalf;
		controlContainerGPanel.addControl(updateCameraDataCheckbox, column1OptionsXPos + controlSpacing, controlC1YPos);

		updateBot1EncoderDataCheckbox = new GCheckbox(parent, 0, 0, columnOptionsWidthEff, controlColumnStandardHeight);
		updateBot1EncoderDataCheckbox.setSelected(false);
		buildGTextIconElement(updateBot1EncoderDataCheckbox, "URGino 1 Odometry");
		controlC1YPos += controlColumnStandardHeight + controlSpacingHalf;
		controlContainerGPanel.addControl(updateBot1EncoderDataCheckbox, column1OptionsXPos + controlSpacing, controlC1YPos);

		updateBot2EncoderDataCheckbox = new GCheckbox(parent, 0, 0, columnOptionsWidthEff, controlColumnStandardHeight);
		updateBot2EncoderDataCheckbox.setSelected(false);
		buildGTextIconElement(updateBot2EncoderDataCheckbox, "URGino 2 Odometry");
		controlC1YPos += controlColumnStandardHeight;
		controlContainerGPanel.addControl(updateBot2EncoderDataCheckbox, column1OptionsXPos + controlSpacing, controlC1YPos);

		updateBot3EncoderDataCheckbox = new GCheckbox(parent, 0, 0, columnOptionsWidthEff, controlColumnStandardHeight);
		updateBot3EncoderDataCheckbox.setSelected(false);
		buildGTextIconElement(updateBot3EncoderDataCheckbox, "URGino 3 Odometry");
		controlC1YPos += controlColumnStandardHeight;
		controlContainerGPanel.addControl(updateBot3EncoderDataCheckbox, column1OptionsXPos + controlSpacing, controlC1YPos);

		drawEncoderUpFrontCheckbox = new GCheckbox(parent, 0, 0, columnOptionsWidthEff, controlColumnStandardHeight);
		drawEncoderUpFrontCheckbox.setSelected(true);
		buildGTextIconElement(drawEncoderUpFrontCheckbox, "Draw Odometry up front");
		controlC1YPos +=  updateHeadingGLabel.getHeight() + controlSpacingHalf;
		controlContainerGPanel.addControl(drawEncoderUpFrontCheckbox, column1OptionsXPos + controlSpacing, controlC1YPos);

		drawEncoderUpFrontCheckbox.setSelected(true);
		
		//draw options column 2: acaBot-Logo
		float acaBotLogoWidth = column3OptionsXPos - column2OptionsXPos - controlSpacing;
		acaBotLogoGImageButton = new GImageButton(parent, 0, 0, acaBotLogoWidth, acaBotLogoWidth * 1.22f, new String[] {"acaBot-Logo.png"});
		acaBotLogoGImageButton.setEnabled(false);
		controlContainerGPanel.addControl(acaBotLogoGImageButton, column2OptionsXPos + controlSpacingHalf, controlYPos - controlSpacingHalf / 2);
		
		// draw options column 3: record data
		controlsFont = FontManager.getPriorityFont(monoSpaced, Font.BOLD, 14);
		recordCameraHeadingGLabel = new GLabel(parent, 0, 0, columnOptionsWidth, controlColumnHeadingHeight);
		recordCameraHeadingGLabel.setTextAlign(GAlign.CENTER, null);
		buildGTextIconElement(recordCameraHeadingGLabel, "Record Camera Data");

		recordCameraGDropList = new GDropList(parent, 0, 0, columnOptionsWidthEff, 4*controlColumnHeadingHeight, 4); // ???
		recordCameraGDropList.setLocalColorScheme(1);
		String[] dropListItems = {NO_SELECTION, URGINO_1, URGINO_2, URGINO_3};
		recordCameraGDropList.setItems(dropListItems, 0);
		recordCameraGDropList.setFont(controlsFont);
		
		recordEncoderHeadingGLabel = new GLabel(parent, 0, 0, columnOptionsWidth, controlColumnHeadingHeight);
		recordEncoderHeadingGLabel.setTextAlign(GAlign.CENTER, null);
		buildGTextIconElement(recordEncoderHeadingGLabel, "Record Encoder Data");
		
		recordEncoderGDropList = new GDropList(parent, 0, 0, columnOptionsWidthEff, 4*controlColumnHeadingHeight, 4); // ???
		recordEncoderGDropList.setLocalColorScheme(1);
		recordEncoderGDropList.setItems(dropListItems, 0);
		recordEncoderGDropList.setFont(controlsFont);

		recordGButton = new GButton(parent, 0, 0, 3.2f * controlColumnStandardHeight, 3 * controlColumnStandardHeight); // Image Array: [off, over, down]
		recordGButton.setText("Record", GAlign.CENTER, GAlign.MIDDLE);
		recordIconGImageButtonRed = new GImageButton(parent, 0, 0, 3 * controlColumnStandardHeight, 1 * controlColumnStandardHeight, new String[] {"isNotAvailable.png"});
		recordIconGImageButtonGreen = new GImageButton(parent, 0, 0, 3 * controlColumnStandardHeight, 1 * controlColumnStandardHeight, new String[] {"isAvailable.png"});
		recordIconGImageButtonRed.setVisible(false);
		recordIconGImageButtonGreen.setVisible(false);

		recordIconGImageButtonRed = new GImageButton(parent, 0, 0, 3 * controlColumnStandardHeight, 1 * controlColumnStandardHeight, new String[] {"isNotAvailable.png"});
		recordIconGImageButtonGreen = new GImageButton(parent, 0, 0, 3 * controlColumnStandardHeight, 1 * controlColumnStandardHeight, new String[] {"isAvailable.png"});
		recordIconGImageButtonRed.setVisible(false);
		recordIconGImageButtonGreen.setVisible(false);
		
		int controlC3YPos = controlYPos + controlSpacing;
		controlContainerGPanel.addControl(recordCameraHeadingGLabel, column3OptionsXPos, controlC3YPos);
		controlC3YPos += recordCameraHeadingGLabel.getHeight() + controlSpacingHalf / 2;
		controlContainerGPanel.addControl(recordCameraGDropList, column3OptionsXPos + controlSpacing, controlC3YPos);
		controlC3YPos += recordEncoderHeadingGLabel.getHeight() + controlSpacingHalf;
		controlContainerGPanel.addControl(recordEncoderHeadingGLabel, column3OptionsXPos, controlC3YPos);
		controlC3YPos += recordEncoderHeadingGLabel.getHeight() + controlSpacingHalf / 2;
		controlContainerGPanel.addControl(recordEncoderGDropList, column3OptionsXPos + controlSpacing, controlC3YPos);
		controlC3YPos += controlColumnHeadingHeight + controlSpacingHalf;
		controlContainerGPanel.addControl(recordGButton, column3OptionsXPos + (columnOptionsWidth - recordGButton.getWidth()) / 2, controlC3YPos);
		controlC3YPos += recordGButton.getHeight() + controlSpacingHalf;
		controlContainerGPanel.addControl(recordIconGImageButtonRed, column3OptionsXPos + (columnOptionsWidth - recordIconGImageButtonRed.getWidth()) / 2, controlC3YPos);
		controlContainerGPanel.addControl(recordIconGImageButtonGreen, column3OptionsXPos + (columnOptionsWidth - recordIconGImageButtonGreen.getWidth()) / 2, controlC3YPos);
	}

	/**
	 * Sets the font and text for a given G4P-element.
	 * @param control The G4P-element to set.
	 * @param text A String containing the text to set. Pass null to don't set any text.
	 */
	private void buildGTextIconElement(GTextIconBase control, String text) {
		control.setFont(controlsFont);
		control.setOpaque(false); // for element placement testing
		if(text != null) {
			control.setText(text);
		}
	}
}
