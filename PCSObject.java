// uncomment Java package declaration when using the PDE


public abstract class PCSObject {
//	private long timerCount;
//
//	public void setTimerCount(Long count) {
//		this.timerCount = count;
//	}
//	
//	public long getTimerCount() {
//		return timerCount;
//	}
	
	public abstract void setXPosition(double xPos);

	public abstract double getXPosition();

	public abstract void setYPosition(double yPos);

	public abstract double getYPosition();
	
	public double getDirection() {
		return 0;
	}
	
	public String getName() {
		return "";
	}
}