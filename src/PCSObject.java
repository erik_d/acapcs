public abstract class PCSObject {
	
	public abstract void setXPosition(double xPos);

	public abstract double getXPosition();

	public abstract void setYPosition(double yPos);

	public abstract double getYPosition();
	
	public double getDirection() {
		return 0;
	}
	
	public String getName() {
		return "";
	}
}
