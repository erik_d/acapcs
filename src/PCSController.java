import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import org.json.simple.JsonArray;
import org.json.simple.JsonObject;
import org.json.simple.Jsoner;

import g4p_controls.GButton;
import g4p_controls.GDropList;
import g4p_controls.GEvent;
import g4p_controls.GTimer;
import g4p_controls.GToggleControl;
import processing.core.PApplet;

/**
 * The Controller gets and parses the data from the source and updates the Model and View.
 * @author Erik Dobberkau
 */
public class PCSController {
	private PApplet parent;
	private PCSModel pcsModel;
	private PCSView pcsView;
	private final String CAMERA_DATA_URL, ENCODER_DATA_URL_1, ENCODER_DATA_URL_2, ENCODER_DATA_URL_3;
	private final String ROBOT_RED_DRAGON = "Red Dragon";
	private final String ROBOT_BLUE_BIRD = "Blue Bird";
	private final String ROBOT_GREEN_FALCON = "Green Falcon";
	private int updateTimerInterval, initialTimerDelay, connectionTimeout, readTimeout;
	private long timerCount;
	private GTimer updateTimer;
	private boolean testMode;
	private EncoderRobot dummy;

	/**
	 * Constructor
	 * @param parent The instance of the Processing main class PApplet which was passed to the Controller.
	 * @param pcsModel The instance of PCSModel which was passed to the Controller.
	 * @param pcsView The instance of PCSView which was passed to the Controller.
	 */
	public PCSController(PApplet parent, PCSModel pcsModel, PCSView pcsView) {
		this.parent = parent;
		this.pcsModel = pcsModel;
		this.pcsView = pcsView;
		CAMERA_DATA_URL = new String("http://pcs-maze.pse.beuth-hochschule.de/api/v1/position/1");
		ENCODER_DATA_URL_1 = new String("http://urgino1.pse.beuth-hochschule.de/api/v1/stat");
		ENCODER_DATA_URL_2 = new String("http://urgino2.pse.beuth-hochschule.de/api/v1/stat");
		ENCODER_DATA_URL_3 = new String("http://urgino3.pse.beuth-hochschule.de/api/v1/stat");
		connectionTimeout = 200;
		readTimeout = 300;
		updateTimerInterval = 200;	// Timer: update every "n" ms
		initialTimerDelay = 1000;	// Timer: delay before first event is fired
		timerCount = 0l;
		initUpdateTimer(updateTimerInterval, initialTimerDelay);
		dummy = new EncoderRobot(true);
		// draw a dummy EncoderRobot 3
		testMode = true;
	}
	
	/**
	 * Creates and starts a G4P Timer for updating Model data.
	 * @param interval Time in ms between calls.
	 * @param initialDelay Initial waiting time in ms after starting the Timer.
	 */
	private void initUpdateTimer(int interval, int initialDelay) {
		updateTimer = new GTimer(parent, this, "updateModel", updateTimerInterval);
		updateTimer.setInitialDelay(initialDelay);
		updateTimer.start();
	}
	
	/**
	 * GTimer Event Listener method, added from GTimer initUpdateTimer(int, int)
	 * @param timer The G4P Timer object.
	 */
	public void updateModel(GTimer timer) {
		timerCount += 1l;
		if(pcsView.updateCameraDataCheckbox.isSelected()) {
			updateModelCameraData(getStringFromUrl(CAMERA_DATA_URL), timerCount);
		} 
		if(pcsView.updateBot1EncoderDataCheckbox.isSelected()) {
			updateModelEncoderData(getStringFromUrl(ENCODER_DATA_URL_1), timerCount, 1);	
		}
		if(pcsView.updateBot2EncoderDataCheckbox.isSelected()) {
			updateModelEncoderData(getStringFromUrl(ENCODER_DATA_URL_2), timerCount, 2);	
		}
		if(pcsView.updateBot3EncoderDataCheckbox.isSelected()) {
			updateModelEncoderData(getStringFromUrl(ENCODER_DATA_URL_3), timerCount, 3);	
		}
	}

	/**
	 *  Updates the View-Model and is called by Processings draw()-method
	 */
	public void updateView() {
		if(PCSModel.hasNewCameraRobots) {
			pcsView.updateCameraRobotList(pcsModel.manageCameraRobots(null));
		}
		if(PCSModel.hasNewEncoderRobot1) {
			pcsView.updateEncoderRobotList1(pcsModel.manageEncoderRobot1(null));
		}
		if(PCSModel.hasNewEncoderRobot2) {
			pcsView.updateEncoderRobotList2(pcsModel.manageEncoderRobot2(null));
		}
		if(PCSModel.hasNewEncoderRobot3) {
			pcsView.updateEncoderRobotList3(pcsModel.manageEncoderRobot3(null));
		}
		if(PCSModel.hasNewCameraObjects) {
			pcsView.updateCameraObjectList(pcsModel.manageCameraObjects(null));
		}
		if(testMode) {
			pcsView.updateEncoderRobotList3(new Tuple<Long, EncoderRobot>(1L, dummy));
		}
	}
	
	/**
	 * Updates Model Camera data (CameraObject and CameraRobot).
	 * @param input String read from Json source containing all Camera data.
	 * @param count A count of tried or done updates.
	 */
	private void updateModelCameraData(String input, Long count) {
		Long timerCount = count;
		JsonObject jsonCameraRootData = null;
		jsonCameraRootData = Jsoner.deserialize(input, new JsonObject());
		updateModelCameraRobots(jsonCameraRootData, timerCount);
		updateModelCameraObjects(jsonCameraRootData, timerCount);
	}

	/**
	 * Updates the Model with CameraRobot data from the passed JsonObject.
	 * @param jsonRootData JsonObject containing Camera data.
	 * @param count A count of tried or done updates.
	 */
	private void updateModelCameraRobots(JsonObject jsonRootData, Long count) {
		Long tCount = count;
		Map<Integer, CameraRobot> cRobotsMap = new HashMap<Integer, CameraRobot>();
		JsonArray jsonCameraRobotArray = jsonRootData.getCollectionOrDefault("robots", new JsonArray());
		for(int i = 0; i < jsonCameraRobotArray.size(); i++) {
			JsonObject robot = (JsonObject)jsonCameraRobotArray.get(i);
				String name = robot.getString("name");
				double xPos = robot.getDouble("x");
				double yPos = robot.getDouble("y");
				double phi = robot.getDouble("phi");
				long timestamp = robot.getLong("timestamp");
			switch (name) {
			case ROBOT_RED_DRAGON:
				cRobotsMap.put(1, new CameraRobot(name, xPos, yPos, phi, timestamp));
				break;
			case ROBOT_BLUE_BIRD:
				cRobotsMap.put(2, new CameraRobot(name, xPos, yPos, phi, timestamp));
				break;
			case ROBOT_GREEN_FALCON:
				cRobotsMap.put(3, new CameraRobot(name, xPos, yPos, phi, timestamp));
				break;
			default:
				cRobotsMap.put(4, new CameraRobot(name, xPos, yPos, phi, timestamp));
				break;
			}
		}
		// create a pair with timerUpdateCount as key and as value a map that contains the robots 
		Tuple<Long, Map<Integer, CameraRobot>> cRobots = new Tuple<Long, Map<Integer, CameraRobot>>(tCount, cRobotsMap);
		pcsModel.manageCameraRobots(cRobots);
	}

	/**
	 * Updates the Model with CameraObject data from the passed JsonObject.
	 * @param jsonRootData JsonObject containing Camera data.
	 * @param count A count of tried or done updates.
	 */
	private void updateModelCameraObjects(JsonObject jsonRootData, Long count) {
		LinkedList<CameraObject> cObjectList = new LinkedList<CameraObject>();
		JsonArray jsonCameraObjectArray = jsonRootData.getCollectionOrDefault("objects", new JsonArray());
		for(int i = 0; i < jsonCameraObjectArray.size(); i++) {
			JsonObject object = (JsonObject)jsonCameraObjectArray.get(i);
				int colorCode = object.getInteger("pixy_cc");
				double xPos = object.getDouble("x");
				double yPos = object.getDouble("y");
				int width = object.getInteger("width");
				int height = object.getInteger("height");
				long timestamp = object.getLong("timestamp");
			cObjectList.add(new CameraObject(colorCode, xPos, yPos, width, height, timestamp));
		}
		Tuple<Long, LinkedList<CameraObject>> objectTuple = new Tuple<Long, LinkedList<CameraObject>>(count, cObjectList);
		pcsModel.manageCameraObjects(objectTuple);
	}

	/**
	 * Updates the specified EncoderRobot Model data from the passed String.
	 * @param input String read from Json source containing all Encoder data.
	 * @param count A count of tried or done updates.
	 * @param id Number of the EncoderRobot to update (URGino 1-3).
	 */
	private void updateModelEncoderData(String input, Long count, int id) {
		JsonObject jsonEncoderRootData = null;
		jsonEncoderRootData = Jsoner.deserialize(input, new JsonObject());
		
		if(jsonEncoderRootData != null && !jsonEncoderRootData.isEmpty()) {
			JsonObject jsonEncoderRobotObject = null;
			jsonEncoderRobotObject = (JsonObject)jsonEncoderRootData.get("pose");
				double xPos = jsonEncoderRobotObject.getDouble("x");
				double yPos = jsonEncoderRobotObject.getDouble("y");
				double phi  = jsonEncoderRobotObject.getDouble("phi");
				long tsPose = jsonEncoderRobotObject.getLong("arduinoTimestamp");
			jsonEncoderRobotObject = (JsonObject)jsonEncoderRootData.get("drive");
				double pulsesLeft = jsonEncoderRobotObject.getDouble("encoderPulsesLeft");
				double pulsesRight = jsonEncoderRobotObject.getDouble("encoderPulsesRight");
				long tsDrive = jsonEncoderRobotObject.getLong("arduinoTimestamp");
			jsonEncoderRobotObject = (JsonObject)jsonEncoderRootData.get("obstacle");
				double lbd30 = jsonEncoderRobotObject.getDouble("leftBeam30Dist");
				double lbd60 = jsonEncoderRobotObject.getDouble("leftBeam60Dist");
				double lbd90 = jsonEncoderRobotObject.getDouble("leftBeam90Dist");
				double lbd45 = jsonEncoderRobotObject.getDouble("leftBeam45Dist");
				double cbd   = jsonEncoderRobotObject.getDouble("centerBeamDist");
				double rbd30 = jsonEncoderRobotObject.getDouble("rightBeam30Dist");
				double rbd60 = jsonEncoderRobotObject.getDouble("rightBeam60Dist");
				double rbd90 = jsonEncoderRobotObject.getDouble("rightBeam90Dist");
				double rbd45 = jsonEncoderRobotObject.getDouble("rightBeam45Dist");
				double [] beamDistArray = {lbd30, lbd60, lbd90, lbd45, cbd, rbd30, rbd60, rbd90, rbd45};
				long tsObstacle = jsonEncoderRobotObject.getLong("unixTimestamp");
			EncoderRobot eRobot = new EncoderRobot(xPos, yPos, phi, tsPose, pulsesLeft, pulsesRight, tsDrive, beamDistArray, tsObstacle);
			Tuple<Long, EncoderRobot> eRobotTuple = new Tuple<Long, EncoderRobot>(count, eRobot);
			switch(id) {
			case 1:
				pcsModel.manageEncoderRobot1(eRobotTuple);
				break;
			case 2:
				pcsModel.manageEncoderRobot2(eRobotTuple);
				break;
			case 3:
				pcsModel.manageEncoderRobot3(eRobotTuple);
				break;
			}
		} else {
			Tuple<Long, EncoderRobot> defaultTuple = new Tuple<Long, EncoderRobot>(count, null);
			switch(id) {
			case 1:
				pcsModel.manageEncoderRobot1(defaultTuple);
				break;
			case 2:
				pcsModel.manageEncoderRobot2(defaultTuple);
				break;
			case 3:
				pcsModel.manageEncoderRobot3(defaultTuple);
				break;
			}
		}
	}

	/**
	 * Builds a String found at the passed url.
	 * @param urlString The url to read from.
	 * @return A String containing the read data.
	 */
	private String getStringFromUrl(String urlString) {
		URL url = null;
		URLConnection con = null;
		InputStreamReader inputStream = null;
		try {
			url = new URL(urlString);
		} catch(MalformedURLException e) {
			System.err.println("Error, malformed url: " + e.getMessage());
			return "";
		}try {
			con = url.openConnection();
			con.setConnectTimeout(connectionTimeout);
			con.setReadTimeout(readTimeout);
			con.connect();
		} catch (SocketTimeoutException e) {
			System.err.println("Connection timeout @ " + urlString + " (" + e.getMessage() + ")");
			return "";
		} catch (IOException e) {
			System.err.println("Connection failure @ " + urlString + " (" + e.getMessage() + ")");
			return "";
		}
		try {
			inputStream = new InputStreamReader(con.getInputStream());			
		} catch (IOException e) {
			System.err.println("Connection failure @ " + urlString + " (" + e.getMessage() + ")");
			return "";
		}try(BufferedReader bufferedReader = new BufferedReader(inputStream)) {
			String inputLine;
			StringBuilder contentStringBuilder = new StringBuilder();
			while((inputLine = bufferedReader.readLine()) != null) {
				contentStringBuilder.append(inputLine);
			}
			return contentStringBuilder.toString();
		} catch (IOException e) {
			System.err.println("Error reading data from: " + urlString + " (" + e.getMessage() + ")");
			return "";
		}
	}
	
	/**
	 * G4P event handling method for buttons.
	 * @param button
	 * @param event
	 */
	public void handleMyButtonEvents(GButton button, GEvent event) {
		if(button == pcsView.recordGButton && event == GEvent.CLICKED && pcsView.recordIconGImageButtonGreen.isVisible()) {
			pcsView.recordIconGImageButtonGreen.setVisible(false);
			pcsView.recordIconGImageButtonRed.setVisible(true);
			if(pcsView.recordCameraGDropList.getSelectedText().equals(pcsView.URGINO_1)) {
				pcsModel.managePCSObjectRecording(null, "startc1");
			} else if(pcsView.recordCameraGDropList.getSelectedText().equals(pcsView.URGINO_2)) {
				pcsModel.managePCSObjectRecording(null, "startc2");
			} else if(pcsView.recordCameraGDropList.getSelectedText().equals(pcsView.URGINO_3)) {
				pcsModel.managePCSObjectRecording(null, "startc3");
			} if(pcsView.recordEncoderGDropList.getSelectedText().equals(pcsView.URGINO_1)) {
				pcsModel.managePCSObjectRecording(null, "starte1");
			} else if(pcsView.recordEncoderGDropList.getSelectedText().equals(pcsView.URGINO_2)) {
				pcsModel.managePCSObjectRecording(null, "starte2");
			} else if(pcsView.recordEncoderGDropList.getSelectedText().equals(pcsView.URGINO_3)) {
				pcsModel.managePCSObjectRecording(null, "starte3");
			}
		} else if(button == pcsView.recordGButton && event == GEvent.CLICKED && pcsView.recordIconGImageButtonRed.isVisible()) {
			pcsView.recordIconGImageButtonRed.setVisible(false);
			pcsView.recordIconGImageButtonGreen.setVisible(true);
			pcsModel.managePCSObjectRecording(null, "stop");
		}
	}
	
	/**
	 * G4P event handling method for droplists.
	 * @param list
	 * @param event
	 */
	public void handlMyDropListEvents(GDropList list, GEvent event) {
		if (list == pcsView.recordCameraGDropList && event == GEvent.SELECTED && pcsView.recordCameraGDropList.getSelectedText().equals(pcsView.URGINO_1)) {
			pcsView.updateCameraDataCheckbox.setSelected(true);
			pcsView.showBot1CameraGCheckbox.setSelected(true);
			pcsView.recordIconGImageButtonGreen.setVisible(true);
		} else if(list == pcsView.recordCameraGDropList && event == GEvent.SELECTED && pcsView.recordCameraGDropList.getSelectedText().equals(pcsView.URGINO_2)) {
			pcsView.updateCameraDataCheckbox.setSelected(true);
			pcsView.showBot2CameraGCheckbox.setSelected(true);
			pcsView.recordIconGImageButtonGreen.setVisible(true);
		} else if(list == pcsView.recordCameraGDropList && event == GEvent.SELECTED && pcsView.recordCameraGDropList.getSelectedText().equals(pcsView.URGINO_3)) {
			pcsView.updateCameraDataCheckbox.setSelected(true);
			pcsView.showBot3CameraGCheckbox.setSelected(true);
			pcsView.recordIconGImageButtonGreen.setVisible(true);
		} else if(list == pcsView.recordCameraGDropList && event == GEvent.SELECTED && pcsView.recordCameraGDropList.getSelectedText().equals(pcsView.NO_SELECTION)) {
			if(pcsView.recordEncoderGDropList.getSelectedText().equals(pcsView.NO_SELECTION)) pcsView.recordIconGImageButtonGreen.setVisible(false);
		} else if (list == pcsView.recordEncoderGDropList && event == GEvent.SELECTED && pcsView.recordEncoderGDropList.getSelectedText().equals(pcsView.URGINO_1)) {
			pcsView.updateBot1EncoderDataCheckbox.setSelected(true);
			pcsView.showBot1EncoderGCheckbox.setSelected(true);
			pcsView.recordIconGImageButtonGreen.setVisible(true);
		} else if(list == pcsView.recordEncoderGDropList && event == GEvent.SELECTED && pcsView.recordEncoderGDropList.getSelectedText().equals(pcsView.URGINO_2)) {
			pcsView.updateBot2EncoderDataCheckbox.setSelected(true);
			pcsView.showBot2EncoderGCheckbox.setSelected(true);
			pcsView.recordIconGImageButtonGreen.setVisible(true);
		} else if(list == pcsView.recordEncoderGDropList && event == GEvent.SELECTED && pcsView.recordEncoderGDropList.getSelectedText().equals(pcsView.URGINO_3)) {
			pcsView.updateBot3EncoderDataCheckbox.setSelected(true);
			pcsView.showBot3EncoderGCheckbox.setSelected(true);
			pcsView.recordIconGImageButtonGreen.setVisible(true);
		} else if(list == pcsView.recordEncoderGDropList && event == GEvent.SELECTED && pcsView.recordEncoderGDropList.getSelectedText().equals(pcsView.NO_SELECTION)) {
			if(pcsView.recordCameraGDropList.getSelectedText().equals(pcsView.NO_SELECTION)) pcsView.recordIconGImageButtonGreen.setVisible(false);
		}
	}
	
	/**
	 * G4P event handling method for checkboxes.
	 * @param checkbox
	 * @param event
	 */
	public void handleMyToggleControlEvents(GToggleControl checkbox, GEvent event) {
		if(checkbox == pcsView.updateCameraDataCheckbox && event == GEvent.DESELECTED) {
			pcsView.showBot1CameraGCheckbox.setSelected(false);
			pcsView.showBot2CameraGCheckbox.setSelected(false);
			pcsView.showBot3CameraGCheckbox.setSelected(false);
			pcsView.showObjectsGCheckbox.setSelected(false);
			pcsView.recordCameraGDropList.setSelected(0);
			if(!pcsView.updateBot1EncoderDataCheckbox.isSelected() && !pcsView.updateBot2EncoderDataCheckbox.isSelected() && !pcsView.updateBot3EncoderDataCheckbox.isSelected()) {
				pcsView.recordIconGImageButtonGreen.setVisible(false);
			}
			pcsView.setCameraRobot1ControlTextColor(pcsView.GREY);
			pcsView.setCameraRobot2ControlTextColor(pcsView.GREY);
			pcsView.setCameraRobot3ControlTextColor(pcsView.GREY);
			pcsView.setCameraObject1ControlTextColor(pcsView.GREY);
			pcsView.setCameraObject2ControlTextColor(pcsView.GREY);
			pcsView.setCameraObject3ControlTextColor(pcsView.GREY);
			pcsView.setCameraObject4ControlTextColor(pcsView.GREY);
		} else if(checkbox == pcsView.showBot1CameraGCheckbox && event == GEvent.SELECTED) {
			pcsView.updateCameraDataCheckbox.setSelected(true);
		} else if(checkbox == pcsView.showBot2CameraGCheckbox && event == GEvent.SELECTED) {
			pcsView.updateCameraDataCheckbox.setSelected(true);
		} else if(checkbox == pcsView.showBot3CameraGCheckbox && event == GEvent.SELECTED) {
			pcsView.updateCameraDataCheckbox.setSelected(true);
		} else if(checkbox == pcsView.showObjectsGCheckbox && event == GEvent.SELECTED) {
			pcsView.updateCameraDataCheckbox.setSelected(true);
		} else if(checkbox == pcsView.updateBot1EncoderDataCheckbox && event == GEvent.DESELECTED) {
			pcsView.showBot1EncoderGCheckbox.setSelected(false);
			pcsView.showBot1LaserGCheckbox.setSelected(false);
			if(pcsView.recordEncoderGDropList.getSelectedText().equals(pcsView.URGINO_1)) pcsView.recordEncoderGDropList.setSelected(0);
			if(!pcsView.updateCameraDataCheckbox.isSelected() && !pcsView.updateBot2EncoderDataCheckbox.isSelected() && !pcsView.updateBot3EncoderDataCheckbox.isSelected()) {
				pcsView.recordIconGImageButtonGreen.setVisible(false);
			}
		} else if(checkbox == pcsView.updateBot2EncoderDataCheckbox && event == GEvent.DESELECTED) {
			pcsView.showBot2EncoderGCheckbox.setSelected(false);
			pcsView.showBot2LaserGCheckbox.setSelected(false);
			if(pcsView.recordEncoderGDropList.getSelectedText().equals(pcsView.URGINO_2)) pcsView.recordEncoderGDropList.setSelected(0);
			if(!pcsView.updateCameraDataCheckbox.isSelected() && !pcsView.updateBot1EncoderDataCheckbox.isSelected() && !pcsView.updateBot3EncoderDataCheckbox.isSelected()) {
				pcsView.recordIconGImageButtonGreen.setVisible(false);
			}
		} else if(checkbox == pcsView.updateBot3EncoderDataCheckbox && event == GEvent.DESELECTED) {
			pcsView.showBot3EncoderGCheckbox.setSelected(false);
			pcsView.showBot3LaserGCheckbox.setSelected(false);
			if(pcsView.recordEncoderGDropList.getSelectedText().equals(pcsView.URGINO_2)) pcsView.recordEncoderGDropList.setSelected(0);
			if(!pcsView.updateCameraDataCheckbox.isSelected() && !pcsView.updateBot1EncoderDataCheckbox.isSelected() && !pcsView.updateBot2EncoderDataCheckbox.isSelected()) {
				pcsView.recordIconGImageButtonGreen.setVisible(false);
			}
		} else if(checkbox == pcsView.showBot1EncoderGCheckbox && event == GEvent.SELECTED) {
			pcsView.updateBot1EncoderDataCheckbox.setSelected(true);
		} else if(checkbox == pcsView.showBot2EncoderGCheckbox && event == GEvent.SELECTED) {
			pcsView.updateBot2EncoderDataCheckbox.setSelected(true);
		} else if(checkbox == pcsView.showBot3EncoderGCheckbox && event == GEvent.SELECTED) {
			pcsView.updateBot3EncoderDataCheckbox.setSelected(true);
		} else if(checkbox == pcsView.showBot1EncoderGCheckbox && event == GEvent.DESELECTED) {
			pcsView.showBot1LaserGCheckbox.setSelected(false);
		} else if(checkbox == pcsView.showBot2EncoderGCheckbox && event == GEvent.DESELECTED) {
			pcsView.showBot2LaserGCheckbox.setSelected(false);
		} else if(checkbox == pcsView.showBot3EncoderGCheckbox && event == GEvent.DESELECTED) {
			pcsView.showBot3LaserGCheckbox.setSelected(false);
		} else if (checkbox == pcsView.showBot1LaserGCheckbox && event == GEvent.SELECTED) {
			pcsView.showBot1EncoderGCheckbox.setSelected(true);
			pcsView.updateBot1EncoderDataCheckbox.setSelected(true);
			pcsView.showERobot1Laser = true;
		} else if (checkbox == pcsView.showBot2LaserGCheckbox && event == GEvent.SELECTED) {
			pcsView.showBot2EncoderGCheckbox.setSelected(true);
			pcsView.updateBot2EncoderDataCheckbox.setSelected(true);
			pcsView.showERobot2Laser = true;
		} else if (checkbox == pcsView.showBot3LaserGCheckbox && event == GEvent.SELECTED) {
			pcsView.showBot3EncoderGCheckbox.setSelected(true);
			pcsView.updateBot3EncoderDataCheckbox.setSelected(true);
			pcsView.showERobot3Laser = true;
		} else if (checkbox == pcsView.showBot1LaserGCheckbox && event == GEvent.DESELECTED) {
			pcsView.showERobot1Laser = false;
		} else if (checkbox == pcsView.showBot2LaserGCheckbox && event == GEvent.DESELECTED) {
			pcsView.showERobot2Laser = false;
		} else if (checkbox == pcsView.showBot3LaserGCheckbox && event == GEvent.DESELECTED) {
			pcsView.showERobot3Laser = false;
		} else if(checkbox == pcsView.drawEncoderUpFrontCheckbox && event == GEvent.SELECTED) {
			pcsView.drawEncoderUpFront = true;
		} else if(checkbox == pcsView.drawEncoderUpFrontCheckbox && event == GEvent.DESELECTED) {
			pcsView.drawEncoderUpFront = false;
		}
	}
}
